@extends('layouts.app')

@section('content')

    <style>
        label{
            font-weight: 800;
        }

        .postImagePreview img{
            width:100%;
            max-height:200px;
        }
    </style>
    <!-- Main Page Main Content-->
    <div class="main-content" id="main-content">

        @include('navs.top')
        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-text">
                            <div class="welcome">
                                <div class="welcome-image">
                                    <img src="img/user.png">
                                </div>
                                <div class="welcome-details">
                                    {{--<div class="welcome-name">Create Task</div>--}}
                                    {{--<div class="welcome-role">CREATE TASK FOR THE STROM APP</div>--}}
                                </div>
                            </div>
                            <div class="all-tasks-section">
                                <div class="container-fluid">

                                    <form class="col-md-12 row" enctype="multipart/form-data" method="post" action="{{url('task/add')}}">
                                        @csrf

                                        <input multiple onchange="readURL(this);" type="file" style="display: none;" id="hiddenFile1"  name="images[]">

                                        <input type="hidden" name="name" value="adminCharge">

                                        <div class="col-6">
                                            <div class="form-group col-md-12">
                                                <label>TASK NAME</label>

                                                <input type="text" class="form-control"
                                                       placeholder="Enter the name of the task" name="name" value="{{ old("task") }}" required autofocus>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label>SHORT DESCRIPTION </label>

                                                <input type="text" class="form-control" aria-describedby="emailHelp"
                                                       placeholder="Briefly describe the task" name="shortDescription" value="{{ old('shortDescription') }}" required autofocus>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label>LONG DESCRIPTION </label>

                                                <textarea class="form-control" name="longDescription" rows="5"{{old('longDescription')}}></textarea>
                                            </div>
                                        </div>

                                        <div class="col-6 row">
                                            <div class="form-group col-md-4">
                                                <label>TYPE</label>

                                                <select class="form-control" style="width: 100%" name="type">
                                                    <option>Social</option>
                                                    <option>Community</option>
                                                </select>

                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Points </label>
                                                <input type="number" min="0" class="form-control"
                                                       placeholder="Enter points" name="points" value="{{ old('points') }}" required autofocus>

                                            </div>

                                            <div class="form-group col-md-4">
                                                <label>Images </label><br>
                                                <a id="addImageButton" class="btn-primary btn btn-sm">Add Images</a>
                                            </div>



                                            <div class="form-group col-md-12">
                                                <label>EXPIRATION DATE</label>

                                                <input type="text" id="expiry" class="form-control"
                                                       placeholder="Enter the name of the task" name="expiry" value="{{ old("expiry") }}" required autofocus>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label>MAX NO OF USERS </label>

                                                <input type="number" min="0" class="form-control"
                                                       placeholder="Enter the highest number of people you want to carry out this task" name="maxUsers" value="{{ old('maxUsers') }}" required autofocus>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label>DURATION OF VERIFICATION</label>

                                                <input type="number" min="1" class="form-control"
                                                       placeholder="Enter duration in days" name="verificationDuration" value="{{ old('verificationDuration') }}" required autofocus>
                                            </div>
                                        </div>

                                        <div class="col-12" align="right" style="float: right">
                                            <button type="submit" class="btn btn-danger">Cancel</button>
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </div>

                                    </form>
                                </div>
                            </div>

                            <div class="row">
                                <div id="preview" class="postImagePreview row"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- End of Page Main Content-->



    <script>

        $(document).ready( function() {

            $('#expiry').datetimepicker();
            $('#addImageButton').on('click',function(){
                $('#hiddenFile1').click();
            });


            // $('#endTime').datetimepicker({
            //     // numberOfMonths: 2,
            //     // showButtonPanel: true
            // });

        } );


        function hover(item){

            console.log('hovered');
            $(item).css('background-color','red');
            $(item).children('img').css('opacity',0.5);
            console.log($(item).children('img'));


        }

        function removeHover(item){
            console.log('cleared');
            $(item).css('background-color','transparent');
            $(item).children('img').css('opacity',1);
        }

        function removeItem(item){
            console.log('removed');
            $(item).remove();
        }

        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').append('<div class="col-md-4 image" onclick="removeItem(this)" onmouseleave="removeHover(this)"  onmouseover="hover(this)"> <img src ="' + e.target.result + '"></div>');
                    };


                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    </script>



@endsection
