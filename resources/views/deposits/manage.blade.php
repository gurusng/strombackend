<?php use Carbon\Carbon; use Illuminate\Support\Facades\Input; ?>
@extends('layouts.app')

@section('content')

    <style>
        label{
            font-weight: 800;
        }
    </style>
    <!-- Main Page Main Content-->
    <div class="main-content" id="main-content">

        @include('navs.top')
        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-text">
                            <div class="welcome">
                                <div class="welcome-image">
                                    <img src="img/user.png">
                                </div>
                                @if(Input::has('status'))
                                    <div class="welcome-details">
                                        <div class="welcome-name">{{Input::get('status')}} Tasks</div>
                                        <div class="welcome-role">LIST OF ALL TASKS {{strtoupper(Input::get('status'))}}</div>
                                    </div>
                                @else
                                    <div class="welcome-details">
                                        <div class="welcome-name">View Deposits</div>
                                        <div class="welcome-role">LIST OF ALL DEPOSITS</div>
                                    </div>
                                @endif
                            </div>
                            <div class="all-tasks-section">
                                <div class="container-fluid">

                                    <div class="col-lg-12 col-md-12 col-sm-12 all-tasks">
                                        All deposits
                                        <form style="float: right;">
                                            <input type="hidden" name="" value="{{request()->input('search')}}">
                                            <input type="text" name="search" value="{{Input::get('search')}}" placeholder="Search by reference">
                                        </form>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Reference</th>
                                                    <th scope="col">User</th>
                                                    <th scope="col">Points</th>
                                                    <th scope="col">Amount(&#x20A6;)</th>
                                                    <th scope="col">Charge(&#x20A6;)</th>
                                                    <th scope="col">Payment Method</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Added By</th>
                                                    <th scope="col">Type</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @if(count($payments) <= 0 )
                                                    <tr>
                                                        <td align="center" colspan="8">
                                                            No Payments
                                                        </td>
                                                    </tr>
                                                @endif
                                                @foreach($payments as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->toDayDateTimeString()}}
                                                            <small>({{$payment->created_at->diffForHumans()}})</small>
                                                        </td>
                                                        <td>{{$payment->reference}}</td>
                                                        <td>{{$payment->User->username}}</td>
                                                        <td>
                                                            <svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                <rect width="23" height="18" fill="url(#pattern0)" />
                                                                <defs>
                                                                    <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                                                        <use xlink:href="#image0" transform="translate(0 -0.00438596) scale(0.0105263 0.0134503)" />
                                                                    </pattern>
                                                                    <image id="image0" width="95" height="75" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF8AAABLCAYAAAAWC1t4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACRFJREFUeNrsXGtwFFUWPncm6SQmgRCEPOSRRCCskifJbunWyqR0sVakjFVWAbrlQgkra1mFaNU+fkjU3Sp3f7gmP9Z1cbeIrkqxVRb44pEAmYCKW+o6EEoXBcRHTAAhA5mANklfz713kpnu6ZnpGWcm6cn9qjo96e6k+37n3O+ce/reAZCQkJCQkJCQkJCQkJCQSAcQOz+8eujGWtwVAMFmEH9TAp89SuMBryQ/UWS/9ZNmJHYJbi78tdaEcONnL24e/PQq7t1KfZdHkh8L4Qcay5C4DbitNvXyyOQbr/Xgvg23HUrtPq8kPxzp3Q2M9Bb8uDoKodbJD+4RAMwIrUrNXq8kP5h49+LHOPEWCCX5daIVQceoehr/Sb8VQ5zC/RqlutM96clXu+qZjm8Z03MjWRl5QAqXAEypE6RnFQNcOg4w4tO3KGcegDMPwHcY6OUTQAc6AL49Ean3tKIBNk5a8tX9dc2ceEIKQgjKLgUy+z4k/iaA7/qAnt2FpH8KdNATuM5IaEY+GgiTobwaINOWooGGgJ75tzBEmOwI903Koj3eSUU+Es+C6RZTAuesBVKyAonbCfSrf4WXkyjHSOGtwgiZRUD7n0XDHTLrBSwbSpkBxp18dV/tar/U6MkqWAxk/qMoF32gHf8T93irREc6RqbciMZcDxRliPb+FUAbMsuImpTrd3vTmnwkng2SPgwhqGgZkPKNQL/4J9C+bXETHfYYxgNyzSNAsitA+/IJNOxnxuxJ9IAkG2DcyFf31jBt/4xrfHD2smATkOlLQDvyG67rP5joSFI0/U4gV98D2ue/QwOcNF7brly3a00yOXCMo+Nv4YOmYE+Yu85P/HqAoU+S/gD0/A6MJZvBMfcvGNQrjKdXqx/9ojntyFc7q1mjdA0j011ASleCdhiJ95kQj8HXOWcdZDRsF2lkogxwYS/Qcy+DYxbGF2duiIOoH99WkDbkI/GsMU/rDmaXAKncBPTY42E9PrNxBzhY5pNVAk40UkJ7wMCrQFH3SUlIqs+etSWdPJ+llWW6h1jQgunf60C/CT/Q1M51B67H1DOR3s8N0N+Khr0WyNRbjKceQu8vSwYRGSmnntINY4HNn1JC3gKgRx+J+GfaF8+Bo+h2/1PngRMHXdr5g0GlBeJPH0jYY3ToSIQb4CDs7HNAZqwDenGfIS0hG/BnwkfAKc121I4qpvPbgzMOR+1mbHQX5txbo2YnzsoWcMxcFp/NcXA2/PEvo97DMftJEQdGDSDOeZWFb06zu+zcofstr1J4PUqOteD4Qdw31vpfsHYP72sY/O8O0X71/8ua7U6+PsOZtQoHUUj88KC1h8VsJy6vv3wCJWqPtWt97wo9yKqI7Dh2Il/ds8gVktej3tP+16zpI/YQlunEg5Hev8VmLN9/gUy52XjYZeeA6zKmlyx3h8FjEByAw8KZH0rSxQ/5FohchqDLrmHVzNHqp1Vc7gFSuBKo/miZeuz2AqXyDa8dyZ+r9/oGHEwds54Z5M3Xa3jfNhg51Wa9vBCTTB3FXlZhdorVotx21Pwyo+fTgRgC6LBPb4zcBcl7UlbpHD5jpvu19s7zg8oFcGXQujcaRr5kSh1k3vBO1PSSv8nCYKtdeDu257tyBogz1yg9CS01pMzzSWGjSy8jmGYOvB9DEPwEKKvpx3JPpRgcU38KzvInwFn8K5hocICNMPLRb0Pkx3JDi+9NeEnCtrJDv/0aIB+93/t+TNIzfPQBcJSuAMKyJeMAfTTTySrmXh9iAOwFGnuHO9nIp+ffc/tnmgkw8jPyY/8/aIAR9lrRwosT5+wHwTHzriAZKoqhq+SmseywUW12SXLvMeKL/2+zyoFe6jEePWVX2fHoBloYQEnxcmM2ET2ITq1H+ag3HVAZjwV7fazEs2zHBLYl/3OdfHg/EBlPTimfoWAVzvmPxl1mYGmnJQPnLMJre8I5kC1lJ2RkyF6ekKubrD9s6cq4iedjJ99ha+Tn3wwQKjmeRJYWUkq+cutRT0i3ZeSXLLc8KHPOWRu//Pc+Yy0GZM7kG69uRnEeuwVcXQN4HZ9NBWR1nmhyc83KuLIj7vH9z4N29hWLg8FVQAffFSUGPZ63e57fBuIdbsAAvVuBlN8P1PPryASe6wbtwv8svzJ0lv9hLNcneawkY+FlSmYRLyVrJ+8LCbTKwjc9tiZfWdrjUTuqWCPGClT0KyS/aDlqvwto0Etys/JCLBOktK/bwVn2ez/5NXyL+A6XXTdjLZ/JAFdO66uhlLalR57PVoYY8n164ikgCx+LW1bMe8puXlgLHt1GfCz0eHJVFSYBLxtPsSDbnhbkKz8/0h6i/ejxbHPU/COh99L62rHHeGD4+MMi4IbN6yuAzFyHvfCPZkG5TfnRzqTM2RyXuZpqZ7ULe0CXTjbQ6x01z4rqJZs8leB5mWGP5VwLjjl/Bnr2JSE5wdey9PK6XXVpVV5A72ee32qUHzY5lr+rrWxJzYNkVwjiBw/xeZsmSOpE2fGdIr63hk0P1y/pzMQeUPV3IRs9DwgZSILnk9xqILM2Ab3QCfT05sA1gf1G5frdrelZWBONbPIHtKAe4OOk06FPwdGwXSwFSmiLczG7ul8Qz1ao9JvGmfZkEz/uns+9ny2QEPofssaWTRcn5Q/x2g9bEiRmKsTv+aRwKZDi9fj/ToLW+1QgpdR7vFtZtKcpJb4HEwDq/jqW93cZF0qMrUBkNZ3iFWIxXP9/UKPRCGy5pxXyc+YJ0tl6LLYU6PSLQC8dCbcykQn/mkmzJmvMAF31ZSDmcdaakCI8d8ZtAChDZNrPxGpEtgyU5fJ88xvjqvncYJx0HFjxVPbiOwADHYFBlvlqxHalqmNNSlUXJhDQAMzzn4Zoq875wra6ANG4J/73s3TUEFfEzAUmMREIH12NvlGp7mxPdXsn5gr07gYXiKWhZVGIg6jnI3/egXtG/KlxyTdgAkM90MiKcC3cCIkln40zHldq97nHNdkDG0A9+GMmQ2yWcPMPIN/r9/Q2pW7/hPjqF7t9304BnwHBvnNHfD9DpC87Yl9qwbZunj4udrtBQkJCQkJCQkJCQkJCQkJCQkJCQkJCwp74XoABAPzI5ErVaIx3AAAAAElFTkSuQmCC" />
                                                                </defs>
                                                            </svg>
                                                            {{ number_format($payment->amount,2) }}
                                                        </td>
                                                        <td>{{number_format($payment->amount,2)}}</td>
                                                        <td>{{ $payment->charge}}</td>
                                                        <td>{{ $payment->channel}}</td>
                                                        <td>

                                                            @if($payment->status == 'initiated')
                                                                <span class="badge badge-warning" style="color:white;padding: 5px 10px;">Initiated</span>
                                                            @endif
                                                            @if($payment->status == 'pending')
                                                                <span class="badge badge-warning" style="color:white;padding: 5px 10px;">Pending</span>
                                                            @endif
                                                            @if($payment->status == 'failed')
                                                                <span class="badge badge-danger" style="color:white;padding: 5px 10px;">Failed</span>
                                                            @endif
                                                            @if($payment->status == 'paid')
                                                                <span class="badge badge-success" style="background-color: #7EDB8F;padding: 5px 10px;">Paid</span>
                                                            @endif

                                                        </td>
                                                        <td>
                                                            @if(!empty($payment->addedBy))
                                                            {{ $payment->Staff->username}}
                                                            @endif
                                                        </td>
                                                        <td>{{$payment->type}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            {{$payments->appends(request()->input())->links()}}
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
