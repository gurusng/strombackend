@extends('layouts.app')

@section('content')

    <style>
        label{
            font-weight: 800;
        }

        .postImagePreview img{
            width:100%;
            max-height:200px;
        }
    </style>
    <!-- Main Page Main Content-->
    <div class="main-content" id="main-content">

        @include('navs.top')
        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-text">
                            <div class="welcome">

                                <div class="welcome-details">
                                    <div class="welcome-name">{{$task->name}}</div>
                                    <div class="welcome-role">Type -

                                        @if($task->type == 'videov2e')
                                            <span class="badge badge-info">View to Earn</span>
                                        @elseif($task->type == 'videop2v')
                                            <span class="badge badge-info">Pay to View</span>
                                            @else
                                            <span class="badge badge-info">{{$task->type}}</span>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="all-tasks-section">
                                <div class="container-fluid row">


                                    <!-- left section -->

                                     @if(strtolower($task->type) == 'community')
                                        <div class="col-md-6 row">

                                        <label>Images </label><br>
                                            @foreach($task->Media as $media)
                                                <div class="col">
                                                    <img src="{{$media->url}}" width="100%">
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif

                                    @if(strtolower($task->type) == 'videop2v' || strtolower($task->type) == 'videov2e')
                                         <div class="col-md-6">

                                         @foreach($task->Media as $media)
                                            <div class="col">
                                                <video style="width: 100%" controls controlsList="nodownload"  webkit-playsinline="webkit-playsinline"  poster="{{$media->thumbnail}}"  playsinline looped preload="auto">
                                                    <source src="{{$media->url}}" type="video/mp4">
                                                </video>

                                            </div>
                                        @endforeach

                                             <div align="center">
                                                 @if($task->isSuspended == 1)
                                                     <a class="btn btn-success" style="background-color: #7EDB8F;border: none; margin-right: 20px;" href="{{route('tasks.approve',['task' => $task->tid])}}">
                                                         Approve
                                                     </a>

                                                     <a class="btn btn-danger" style="background-color: darkred;border: none" href="{{route('tasks.reject',['task' => $task->tid])}}">
                                                         Reject
                                                     </a>
                                                 @endif
                                             </div>
                                         </div>
                                    @endif

                                    <!-- end left section -->


                                    <div class="col-md-6">

                                        <div class="form-group col-md-12">
                                            <label>USERNAME </label>

                                            <p>{{$task->User->username}}</p>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>Name </label>

                                            <p>{{$task->User->name}}</p>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>Phone </label>

                                            <p>{{$task->User->phone}}</p>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>Email </label>
                                            <p>{{$task->User->email}}</p>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>DESCRIPTION </label>

                                            <p>{{$task->longDescription}}</p>
                                        </div>


                                        <div class="form-group col-md-4">
                                            <label>Points </label>
                                            <p>{{ $task->points }}</p>

                                        </div>




                                        <div class="form-group col-md-12">
                                            <label>EXPIRATION DATE</label>
                                            <p>{{ $task->expiry }}</p>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>MAX NO OF USERS </label>

                                            <p>{{ $task->maxUsers }}</p>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>DURATION OF VERIFICATION</label>
                                            <p>{{ $task->verificationDuration }}</p>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>Created</label>
                                            <p>
                                                {{$task->expiry->toDayDateTimeString()}}
                                                ({{$task->expiry->diffForHumans()}})
                                            </p>
                                        </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- End of Page Main Content-->





@endsection
