<?php use Carbon\Carbon; use Illuminate\Support\Facades\Input; ?>
@extends('layouts.app')

@section('content')

    <style>
        label{
            font-weight: 800;
        }
    </style>
    <!-- Main Page Main Content-->
    <div class="main-content" id="main-content">

        @include('navs.top')
        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-text">
                            <div class="welcome">
                                {{--<div class="welcome-image">--}}
                                    {{--<img src="img/user.png">--}}
                                {{--</div>--}}
                                @if(Input::has('status'))
                                    <div class="welcome-details">
                                        <div class="welcome-name">{{Input::get('status')}} Tasks</div>
                                        <div class="welcome-role">LIST OF ALL TASKS {{strtoupper(Input::get('status'))}}</div>
                                    </div>
                                @else
                                    <div class="welcome-details">
                                        <div class="welcome-name">View Tasks</div>
                                        <div class="welcome-role">LIST OF ALL TASKS</div>
                                    </div>
                                @endif
                            </div>
                            <div class="all-tasks-section">
                                <div class="container-fluid">

                                    <div class="col-lg-12 col-md-12 col-sm-12 all-tasks">
                                        All tasks
                                        <form style="float: right;">
                                            <input type="hidden" name="type" value="{{request()->input('type')}}">
                                            <input type="text" name="search" value="{{Input::get('search')}}" placeholder="Search">
                                        </form>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Created Task</th>
                                                    <th scope="col">Created By (username)</th>
                                                    <th scope="col">Type</th>
                                                    <th scope="col">Task Status</th>
                                                    <th scope="col">Expiry</th>
                                                    <th scope="col">No Of Users</th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @if(count($tasks) <= 0 )
                                                    <tr>
                                                        <td align="center" colspan="8">
                                                            No tasks
                                                        </td>
                                                    </tr>
                                                @endif
                                                @foreach($tasks as $task)
                                                    <tr>
                                                        <td>{{$task->created_at->diffForHumans()}}</td>
                                                        <td>
                                                            {{$task->name}}
                                                        </td>
                                                        <td>
                                                            {{$task->User->username}}
                                                        </td>
                                                        <td>
                                                            {{$task->type}}
                                                        </td>
                                                        @if($task->status == 'Completed')
                                                            <td class="completed-task">Completed</td>
                                                        @endif
                                                        @if($task->status == 'In Progress')
                                                            <td class="in-progress-task">In Progress</td>
                                                        @endif
                                                        @if($task->status == 'Cancelled')
                                                            <td class="cancelled-task">Cancelled</td>
                                                        @endif
                                                        <td>
                                                            {{Carbon::createFromFormat("Y-m-d H:i:s",$task->expiry)->diffForHumans()}}
                                                        </td>
                                                        <td>
                                                            {{$task->maxUsers}}
                                                        </td>

                                                        <td>
                                                            <a class="badge badge-success" style="background-color: #7EDB8F;" href="{{url('task/' . $task->tid)}}">
                                                                View
                                                            </a>

                                                            <a class="badge badge-danger" href="{{route('tasks.destroy',$task->tid)}}">
                                                                Delete
                                                            </a>

                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            {{$tasks->appends(request()->input())->links()}}
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Page Main Content-->

    <script>
        $(document).ready( function() {

            $('#expiry').datetimepicker();
            // $('#endTime').datetimepicker({
            //     // numberOfMonths: 2,
            //     // showButtonPanel: true
            // });

        } );
    </script>



@endsection
