<?php use Carbon\Carbon; use Illuminate\Support\Facades\Input; ?>
@extends('dashboard.layouts.app')

@section('content')

    <style>
        label{
            font-weight: 800;
        }
    </style>
    <!-- Main Page Main Content-->
    <div class="main-content" id="main-content">

        @include('dashboard.users.navs.top')
        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-text">
                            <div class="welcome">
                                <div class="welcome-image">
                                    <img src="img/user.png">
                                </div>
                                @if(Input::has('status'))
                                    <div class="welcome-details">
                                        <div class="welcome-name">{{Input::get('status')}} Tasks</div>
                                        <div class="welcome-role">LIST OF ALL TASKS {{strtoupper(Input::get('status'))}}</div>
                                    </div>
                                @else
                                    <div class="welcome-details">
                                        <div class="welcome-name">View Tasks</div>
                                        <div class="welcome-role">LIST OF ALL TASKS</div>
                                    </div>
                                @endif
                            </div>
                            <div class="all-tasks-section">
                                <div class="container-fluid">

                                    <div class="col-lg-12 col-md-12 col-sm-12 all-tasks">
                                        All tasks
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Created Task</th>
                                                    <th scope="col">Type</th>
                                                    <th scope="col">Task Status</th>
                                                    <th scope="col">Expiry</th>
                                                    <th scope="col">No Of Users</th>
                                                    <th scope="col">Suspend</th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @if(count($tasks) <= 0 )
                                                    <tr>
                                                        <td align="center" colspan="8">
                                                            No tasks
                                                        </td>
                                                    </tr>
                                                @endif
                                                @foreach($tasks as $task)
                                                    <tr>
                                                        <td>{{$task->created_at->diffForHumans()}}</td>
                                                        <td>
                                                            {{$task->name}}
                                                        </td>
                                                        <td>
                                                            {{$task->type}}
                                                        </td>
                                                        @if($task->status == 'Completed')
                                                            <td class="completed-task">Completed</td>
                                                        @endif
                                                        @if($task->status == 'In Progress')
                                                            <td class="in-progress-task">In Progress</td>
                                                        @endif
                                                        @if($task->status == 'Cancelled')
                                                            <td class="cancelled-task">Cancelled</td>
                                                        @endif
                                                        <td>
                                                            {{Carbon::createFromFormat("Y-m-d H:i:s",$task->expiry)->diffForHumans()}}
                                                        </td>
                                                        <td>
                                                            {{$task->maxUsers}}
                                                        </td>
                                                        <td>
                                                            <a href="{{url('task/' . $task->tid . '/suspend')}}">
                                                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M7.5 0C3.3645 0 0 3.3645 0 7.5C0 11.6355 3.3645 15 7.5 15C11.6355 15 15 11.6355 15 7.5C15 3.3645 11.6355 0 7.5 0ZM7.5 14.5C3.64025 14.5 0.5 11.3597 0.5 7.5C0.5 3.64025 3.64025 0.5 7.5 0.5C11.3597 0.5 14.5 3.64025 14.5 7.5C14.5 11.3597 11.3597 14.5 7.5 14.5Z"
                                                                          fill="#4C4848" />
                                                                    <path d="M8.25 11.5H10.25V3.5H8.25V11.5ZM8.75 4H9.75V11H8.75V4Z" fill="#4C4848" />
                                                                    <path d="M4.75 11.5H6.75V3.5H4.75V11.5ZM5.25 4H6.25V11H5.25V4Z" fill="#4C4848" />
                                                                </svg>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a class="badge badge-success" style="background-color: #7EDB8F;padding: 5px 10px;" href="{{url('task/' . $task->tid)}}">
                                                                View
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Page Main Content-->

    <script>
        $(document).ready( function() {

            $('#expiry').datetimepicker();
            // $('#endTime').datetimepicker({
            //     // numberOfMonths: 2,
            //     // showButtonPanel: true
            // });

        } );
    </script>



@endsection
