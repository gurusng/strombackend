<nav class="main-content-overlay navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-default">
            <svg width="30" height="30" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.59696 2H15.4035C15.7337 2 16 1.55267 16 1C16 0.448 15.7317 0 15.4035 0H6.59696C6.26634 0 6 0.448 6 1C6 1.55267 6.26827 2 6.59696 2Z"
                      fill="#FAC235" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M15.1579 6H0.842105C0.427923 6 0 6.448 0 7C0 7.55267 0.427923 8 0.842105 8H15.1579C15.5721 8 16 7.55267 16 7C16 6.448 15.5721 6 15.1579 6Z"
                      fill="#FAC235" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M15 12H7C6.26634 12 6 12.4473 6 13C6 13.552 6.26827 14 7 14H15C15.7337 14 16 13.552 16 13C16 12.4473 15.7337 12 15 12Z"
                      fill="#FAC235" />
            </svg>
        </button>

        <!-- <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
          data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
          aria-label="Toggle navigation">
          <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.59696 2H15.4035C15.7337 2 16 1.55267 16 1C16 0.448 15.7317 0 15.4035 0H6.59696C6.26634 0 6 0.448 6 1C6 1.55267 6.26827 2 6.59696 2Z"
              fill="white" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M15.1579 6H0.842105C0.427923 6 0 6.448 0 7C0 7.55267 0.427923 8 0.842105 8H15.1579C15.5721 8 16 7.55267 16 7C16 6.448 15.5721 6 15.1579 6Z"
              fill="white" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M15 12H7C6.26634 12 6 12.4473 6 13C6 13.552 6.26827 14 7 14H15C15.7337 14 16 13.552 16 13C16 12.4473 15.7337 12 15 12Z"
              fill="white" />
          </svg>
        </button> -->

        <div class="ml-auto" id="navbarSupportedContent">
            <ul class="nav navbar-nav navbar-column">
                <li class="nav-item active">
                    <a class="nav-link" href="#">
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.900985" fill-rule="evenodd" clip-rule="evenodd" d="M11.0872 8.99408V6.76065C11.0872 4.68993 9.77917 2.94298 7.94789 2.2938V1.56313C7.94789 0.68952 7.24355 0 6.39888 0C5.53302 0 4.84961 0.710636 4.84961 1.56313V2.2938C3.01859 2.94298 1.71054 4.68993 1.71054 6.76065V8.99408C0.643698 9.7862 0 10.8415 0 11.9993V14.3957H4.8099C4.8099 15.2894 5.53409 16 6.39996 16C7.28568 16 7.99002 15.2691 7.99002 14.3957H12.7996V11.9982C12.8176 10.8415 12.153 9.7862 11.0872 8.99408Z"
                                  fill="white" />
                            <path d="M13 11C15.2091 11 17 9.20914 17 7C17 4.79086 15.2091 3 13 3C10.7909 3 9 4.79086 9 7C9 9.20914 10.7909 11 13 11Z"
                                  fill="#F96D74" stroke="#504EAD" stroke-width="2" />
                        </svg>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('logout')}}">Logout
                        <svg aria-hidden="true" data-prefix="fas" data-icon="sign-out-alt" class="svg-inline--fa fa-sign-out-alt fa-w-16"
                             role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"
                                  fill="#FABD30"></path>
                        </svg>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid profiler-container">
        <div class="profiler">
            <div class="profile-image">
                <img src="{{url('img/Ellipse 2.png')}}">
            </div>
            <div class="profiler-details">
                <div class="profiler-name">{{auth()->user()->name}}</div>
                <div class="profiler-role">{{title_case(auth()->user()->role)}}</div>
            </div>
        </div>
    </div>
    @include('notification')
</nav>