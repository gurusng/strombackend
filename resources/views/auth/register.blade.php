
@extends('layouts.auth')

@section('content')

    <body id="login-page">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><img src="img/logo.svg" class="w-100"></a>
            <div class="collapse d-none d-md-block" id="navbar-action-buttons">
                <div class="btn-group ml-auto" role="group" aria-label="Basic example">
                    {{--<a href="{{url('login')}}" type="button" class="btn btn-secondary" style="text-decoration: none">Sign In</a>--}}
                    {{--<a href="#" type="button" class="btn btn-primary" style="text-decoration: none">Sign Up</a>--}}
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="d-none d-md-block col-md-6 blue-bg">
                <div class="d-flex justify-content-center align-items-center block slider-section h-100">
                    <div id="signin-carousel" class="carousel slide mt-3" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#signin-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#signin-carousel" data-slide-to="1"></li>
                            <li data-target="#signin-carousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="carousel-caption d-none d-md-block">
                                    <p class="lead onboard-title">
                                        Make meaningful and profitable use of your online time.
                                    </p>
                                </div>
                                <img class="d-block w-100" src="img/signin-illustration.svg" alt="Signin Illustration">
                            </div>
                            <div class="carousel-item">
                                <div class="carousel-caption d-none d-md-block">
                                    <p class="lead onboard-title">
                                        Utilize your social accounts for
                                        rewards and points
                                    </p>
                                </div>
                                <img class="d-block w-100" src="img/signin-illustration.svg" alt="Signin Illustration">
                            </div>
                            <div class="carousel-item">
                                <div class="carousel-caption d-none d-md-block">
                                    <p class="lead onboard-title">
                                        Make meaningful and profitable use of your online time.
                                    </p>
                                </div>
                                <img class="d-block w-100" src="img/signin-illustration.svg" alt="Signin Illustration">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="col-12 col-md-6 yellow-bg">
                <div class="block d-flex flex-row flex-wrap h-100">
                    <div class="w-100 flex-fill justify-content-center d-flex h-100 form-holder">
                        <form method="POST" action="{{ route('register') }}" class="mt-3 w-100">
                            @csrf

                            <h2 class="form-title mb-5">
                                <a class="font-weight-light active" href="{{url('register')}}">Sign Up</a>
                            </h2>
                            @include('notification')

                            <div class="form-group">
                                <label for="name">Username</label>
                                <input type="text" class="form-control" id="name" aria-describedby="textHelp"
                                       placeholder="Enter a unique username" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <div class="alert alert-danger" align="center">{{$errors->first('username')}}</div>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" aria-describedby="textHelp"
                                       placeholder="Enter your full name" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger" align="center">{{$errors->first('name')}}</div>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" id="name" aria-describedby="phoneHelp"
                                       placeholder="Enter your phone number" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <div class="alert alert-danger" align="center">{{$errors->first('phone')}}</div>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">EMAIL</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                       placeholder="Your email goes here" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <div class="alert alert-danger" align="center">{{$errors->first('email')}}</div>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">PASSWORD</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="**********" name="password" required>
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger" align="center">{{$errors->first('password')}}</div>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="password-confirm">CONFIRM PASSWORD</label>
                                <input type="password" class="form-control" id="password-confirm" placeholder="**********" name="password_confirmation" required>
                                @if ($errors->has('password_confirmation'))
                                    <div class="alert alert-danger" align="center">{{$errors->first('password_confirmation')}}</div>
                                @endif

                            </div>
                            <div class="clearfix pt-3">
                                <button type="submit" class="btn btn-primary mr-2">Sign Up</button>
                                <a href="{{url('register')}}">Don't have an account?</a>
                            </div>
                        </form>
                    </div>
                    <footer class="flex-fill align-self-end">
                        <ul class="list-inline footnotes">
                            <li class="list-inline-item">
                                <a href="javascript:;">
                                    Terms & Conditions
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:;">
                                    Services
                                </a>
                            </li>
                        </ul>
                    </footer>
                </div>
            </section>
        </div>
    </div>


    @endsection




{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Register') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('register') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Register') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endsection--}}
