@extends('layouts.auth')

@section('content')

    <body id="login-page">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><img src="img/logo.svg" class="w-100"></a>
            <div class="collapse d-none d-md-block" id="navbar-action-buttons">
                <div class="btn-group ml-auto" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-primary">Sign In</button>
                    {{--<a href="{{url('register')}}" type="button" class="btn btn-secondary" style="text-decoration: none">Sign Up</a>--}}
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="d-none d-md-block col-md-6 blue-bg">
                <div class="d-flex justify-content-center align-items-center block slider-section h-100">
                    <div id="signin-carousel" class="carousel slide mt-3" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#signin-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#signin-carousel" data-slide-to="1"></li>
                            <li data-target="#signin-carousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="carousel-caption d-none d-md-block">
                                    <p class="lead onboard-title">
                                        Make meaningful and profitable use of your online time.
                                    </p>
                                </div>
                                <img class="d-block w-100" src="img/signin-illustration.svg" alt="Signin Illustration">
                            </div>
                            <div class="carousel-item">
                                <div class="carousel-caption d-none d-md-block">
                                    <p class="lead onboard-title">
                                        Utilize your social accounts for
                                        rewards and points
                                    </p>
                                </div>
                                <img class="d-block w-100" src="img/signin-illustration.svg" alt="Signin Illustration">
                            </div>
                            <div class="carousel-item">
                                <div class="carousel-caption d-none d-md-block">
                                    <p class="lead onboard-title">
                                        Make meaningful and profitable use of your online time.
                                    </p>
                                </div>
                                <img class="d-block w-100" src="img/signin-illustration.svg" alt="Signin Illustration">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="col-12 col-md-6 yellow-bg">
                <div class="block d-flex flex-row flex-wrap h-100">

                    <div class="w-100 flex-fill justify-content-center d-flex h-100 form-holder">

                        <form method="POST" action="{{ route('login') }}" class="mt-3 w-100">
                                @csrf

                            <h2 class="form-title mb-5">
                                <span class="font-weight-light active">Sign In</span>
                            </h2>
                            @include('notification')

                            <div class="form-group">
                                <label for="exampleInputEmail1">EMAIL</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                       placeholder="Your email goes here" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <div class="alert alert-danger" align="center">{{$errors->first('email')}}</div>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">PASSWORD</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="**********" name="password" required>
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger" align="center">{{$errors->first('password')}}</div>
                                @endif

                            </div>
                            <div class="clearfix pt-3">
                                <button type="submit" class="btn btn-primary mr-2">Sign In</button>
                                <a href="{{url('register')}}">Don't have an account?</a>
                            </div>
                        </form>
                    </div>
                    <footer class="flex-fill align-self-end">
                        <ul class="list-inline footnotes">
                            <li class="list-inline-item">
                                <a href="javascript:;">
                                    Terms & Conditions
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:;">
                                    Services
                                </a>
                            </li>
                        </ul>
                    </footer>
                </div>
            </section>
        </div>
    </div>


@endsection
