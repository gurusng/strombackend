<?php use Carbon\Carbon; use Illuminate\Support\Facades\Input; ?>
@extends('layouts.app')

@section('content')

    <style>
        label{
            font-weight: 800;
        }
    </style>
    <!-- Main Page Main Content-->
    <div class="main-content" id="main-content">

        @include('navs.top')
        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-text">
                            <div class="welcome">
                                <div class="welcome-image">
                                </div>
                                <div class="welcome-details">
{{--                                    <div class="welcome-name">QUIDPAY BALANCE : &#x20A6;{{$balance}}</div>--}}
                                </div>
                            </div>
                            <div class="all-tasks-section">
                                <div class="container-fluid">

                                    <div class="col-lg-12 col-md-12 col-sm-12 all-tasks">
                                        SUMMARY
                                        <form style="float: right;">
                                            <input type="hidden" name="" value="{{request()->input('search')}}">
                                            <input type="text" name="search" value="{{Input::get('search')}}" placeholder="Search">
                                        </form>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col"></th>
                                                    <th scope="col">Day(&#x20A6;)</th>
                                                    <th scope="col">Week(&#x20A6;)</th>
                                                    <th scope="col">Month(&#x20A6;)</th>
                                                    <th scope="col">All Time(&#x20A6;)</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <td>Admin Charges</td>
                                                        <td>{{number_format($paymentsToday,2)}}</td>
                                                        <td>
                                                            {{number_format($paymentsThisWeek,2)}}
                                                        </td>
                                                        <td>
                                                            {{number_format($paymentsThisMonth,2)}}
                                                        </td>
                                                        <td>
                                                            {{number_format($allPayments,2)}}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Deposit Charges</td>
                                                        <td>{{number_format($chargesToday,2)}}</td>
                                                        <td>
                                                            {{number_format($chargesThisWeek ,2)}}
                                                        </td>
                                                        <td>
                                                            {{number_format($chargesThisMonth,2)}}
                                                        </td>
                                                        <td>
                                                            {{number_format($allCharges,2)}}
                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <td>Net Revenue</td>
                                                        <td>{{number_format($paymentsToday - $chargesToday,2)}}</td>
                                                        <td>
                                                            {{number_format($paymentsThisWeek -  $chargesThisWeek,2)}}
                                                        </td>
                                                        <td>
                                                            {{number_format($paymentsThisMonth - $chargesThisMonth,2)}}
                                                        </td>
                                                        <td>
                                                            {{number_format($allPayments - $allCharges,2)}}
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
