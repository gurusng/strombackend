@extends('layouts.app')

@section('content')
    <!-- Main Page Main Content-->
    <div class="main-content" id="main-content">

        @include('navs.top')
        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-text">
                            <div class="welcome">
                                <div class="welcome-image">
                                    <img src="img/user.png">
                                </div>
                                <div class="welcome-details">
                                    <div class="welcome-name">Settings</div>
                                    <div class="welcome-role">CHANGE APPLICATION SETTINGS</div>
                                </div>
                            </div>
                            <div class="all-tasks-section">
                                <div class="container-fluid">
                                    <div class="col-lg-12 col-md-12 col-sm-12 manage-users">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Setting</th>
                                                    <th>Value</th>
                                                    <th>Last Changed</th>
                                                </tr>
                                                <tr>
                                                    <td>Admin Charge</td>
                                                    <td>{{$adminCharge->value}}</td>
                                                    <td>
                                                        {{$adminCharge->updated_at->toDayDateTimeString()}}
                                                        <small>({{$adminCharge->updated_at->diffForHumans()}})</small>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </div>

                                    <br>

                                    <div class="row">
                                        <form class="col-md-4" method="post" action="{{url('settings/add')}}">
                                            @csrf
                                            <input type="hidden" name="name" value="adminCharge">
                                            <div class="form-group">
                                                <label>ADMINISTRATIVE CHARGE</label>

                                                <input type="number" step="0.01" max="100" min="0" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                                       placeholder="Enter the percentage value" name="value" value="{{ $adminCharge->value }}" required autofocus>
                                            </div>

                                            <button type="submit" class="btn btn-success">Save</button>
                                        </form>

                                        <div class="col-md-4">
                                            <a href="{{route('settings.resendUnverifiedEmails')}}" class="btn btn-primary">Resend Unverified Emails</a>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Page Main Content-->



@endsection
