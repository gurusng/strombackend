@extends('layouts.app')

@section('content')
    <!-- Main Page Main Content-->
    <div class="main-content" id="main-content">

        @include('navs.top')
        <div class="content-body">
            <div class="container-fluid">

                <!-- user info -->
                <div class="row">
                    <div class="container revenue-count pt-3">
                        USERS
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-day">
                                    <p>Today</p>
                                    <p>{{count($usersToday)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-week">
                                    <p>This Week</p>
                                    <p>{{count($usersThisWeek)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-month">
                                    <p>This Month</p>
                                    <p>{{count($usersThisMonth)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-alltime">
                                    <p>All Time</p>
                                    <p>{{count($allUsers)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- task info -->
                <div class="row">
                    <div class="container revenue-count pt-3">
                        TASKS
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-day">
                                    <p>Today</p>
                                    <p>{{count($tasksToday)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-week">
                                    <p>This Week</p>
                                    <p>{{count($tasksThisWeek)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-month">
                                    <p>This Month</p>
                                    <p>{{count($tasksThisMonth)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-alltime">
                                    <p>All Time</p>
                                    <p>{{count($allTasks)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- payment info -->
                <div class="row">
                    <div class="container revenue-count pt-3">
                        DEPOSITS
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-day">
                                    <p>Today</p>
                                    <p>&#x20A6;{{number_format($paymentsToday,2)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-week">
                                    <p>This Week</p>
                                    <p>&#x20A6;{{number_format($paymentsThisWeek,2)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-month">
                                    <p>This Month</p>
                                    <p>&#x20A6;{{number_format($paymentsThisMonth,2)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-alltime">
                                    <p>All Time</p>
                                    <p>&#x20A6;{{number_format($allPayments,2)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- cashout info -->
                <div class="row">
                    <div class="container revenue-count pt-3">
                        CASHOUTS
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-day">
                                    <p>Today</p>
                                    <p>&#x20A6;{{number_format($cashoutsToday,2)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-week">
                                    <p>This Week</p>
                                    <p>&#x20A6;{{number_format($cashoutsThisWeek,2)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-month">
                                    <p>This Month</p>
                                    <p>&#x20A6;{{number_format($cashoutsThisMonth,2)}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="revenue-alltime">
                                    <p>All Time</p>
                                    <p>&#x20A6;{{number_format($allCashouts,2)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="container-text">

                            <div class="all-tasks-section">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12 all-tasks">
                                            Recent tasks
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">Created Task</th>
                                                        <th scope="col">Type</th>
                                                        <th scope="col">Task Status</th>
                                                        <th scope="col"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($tasks as $task)
                                                    <tr>
                                                        <td>{{$task->created_at->diffForHumans()}}</td>
                                                        <td>
                                                            {{$task->name}}
                                                        </td>
                                                        <td>
                                                            {{$task->type}}
                                                        </td>
                                                        @if($task->status == 'Completed')
                                                            <td class="completed-task">Completed</td>
                                                        @endif
                                                        @if($task->status == 'In Progress')
                                                            <td class="in-progress-task">In Progress</td>
                                                        @endif
                                                        @if($task->status == 'Cancelled')
                                                            <td class="cancelled-task">Cancelled</td>
                                                        @endif
                                                        <td>
                                                            <a class="badge badge-danger" href="{{route('tasks.destroy',$task->tid)}}">
                                                                Delete
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 cash-out">
                                            RECENT CASHOUTS
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">User</th>
                                                        <th scope="col">Points</th>
                                                        <th scope="col">Amount(&#x20A6;)</th>
                                                        <th scope="col">Type</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($cashouts as $cashout)
                                                    <tr>
                                                        <td>{{$cashout->created_at->diffForHumans()}}</td>
                                                        <td>{{$cashout->User->username}}</td>
                                                        <td>
                                                            <svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                <rect width="23" height="18" fill="url(#pattern0)" />
                                                                <defs>
                                                                    <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                                                        <use xlink:href="#image0" transform="translate(0 -0.00438596) scale(0.0105263 0.0134503)" />
                                                                    </pattern>
                                                                    <image id="image0" width="95" height="75" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF8AAABLCAYAAAAWC1t4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACRFJREFUeNrsXGtwFFUWPncm6SQmgRCEPOSRRCCskifJbunWyqR0sVakjFVWAbrlQgkra1mFaNU+fkjU3Sp3f7gmP9Z1cbeIrkqxVRb44pEAmYCKW+o6EEoXBcRHTAAhA5mANklfz713kpnu6ZnpGWcm6cn9qjo96e6k+37n3O+ce/reAZCQkJCQkJCQkJCQkJCQSAcQOz+8eujGWtwVAMFmEH9TAp89SuMBryQ/UWS/9ZNmJHYJbi78tdaEcONnL24e/PQq7t1KfZdHkh8L4Qcay5C4DbitNvXyyOQbr/Xgvg23HUrtPq8kPxzp3Q2M9Bb8uDoKodbJD+4RAMwIrUrNXq8kP5h49+LHOPEWCCX5daIVQceoehr/Sb8VQ5zC/RqlutM96clXu+qZjm8Z03MjWRl5QAqXAEypE6RnFQNcOg4w4tO3KGcegDMPwHcY6OUTQAc6AL49Ean3tKIBNk5a8tX9dc2ceEIKQgjKLgUy+z4k/iaA7/qAnt2FpH8KdNATuM5IaEY+GgiTobwaINOWooGGgJ75tzBEmOwI903Koj3eSUU+Es+C6RZTAuesBVKyAonbCfSrf4WXkyjHSOGtwgiZRUD7n0XDHTLrBSwbSpkBxp18dV/tar/U6MkqWAxk/qMoF32gHf8T93irREc6RqbciMZcDxRliPb+FUAbMsuImpTrd3vTmnwkng2SPgwhqGgZkPKNQL/4J9C+bXETHfYYxgNyzSNAsitA+/IJNOxnxuxJ9IAkG2DcyFf31jBt/4xrfHD2smATkOlLQDvyG67rP5joSFI0/U4gV98D2ue/QwOcNF7brly3a00yOXCMo+Nv4YOmYE+Yu85P/HqAoU+S/gD0/A6MJZvBMfcvGNQrjKdXqx/9ojntyFc7q1mjdA0j011ASleCdhiJ95kQj8HXOWcdZDRsF2lkogxwYS/Qcy+DYxbGF2duiIOoH99WkDbkI/GsMU/rDmaXAKncBPTY42E9PrNxBzhY5pNVAk40UkJ7wMCrQFH3SUlIqs+etSWdPJ+llWW6h1jQgunf60C/CT/Q1M51B67H1DOR3s8N0N+Khr0WyNRbjKceQu8vSwYRGSmnntINY4HNn1JC3gKgRx+J+GfaF8+Bo+h2/1PngRMHXdr5g0GlBeJPH0jYY3ToSIQb4CDs7HNAZqwDenGfIS0hG/BnwkfAKc121I4qpvPbgzMOR+1mbHQX5txbo2YnzsoWcMxcFp/NcXA2/PEvo97DMftJEQdGDSDOeZWFb06zu+zcofstr1J4PUqOteD4Qdw31vpfsHYP72sY/O8O0X71/8ua7U6+PsOZtQoHUUj88KC1h8VsJy6vv3wCJWqPtWt97wo9yKqI7Dh2Il/ds8gVktej3tP+16zpI/YQlunEg5Hev8VmLN9/gUy52XjYZeeA6zKmlyx3h8FjEByAw8KZH0rSxQ/5FohchqDLrmHVzNHqp1Vc7gFSuBKo/miZeuz2AqXyDa8dyZ+r9/oGHEwds54Z5M3Xa3jfNhg51Wa9vBCTTB3FXlZhdorVotx21Pwyo+fTgRgC6LBPb4zcBcl7UlbpHD5jpvu19s7zg8oFcGXQujcaRr5kSh1k3vBO1PSSv8nCYKtdeDu257tyBogz1yg9CS01pMzzSWGjSy8jmGYOvB9DEPwEKKvpx3JPpRgcU38KzvInwFn8K5hocICNMPLRb0Pkx3JDi+9NeEnCtrJDv/0aIB+93/t+TNIzfPQBcJSuAMKyJeMAfTTTySrmXh9iAOwFGnuHO9nIp+ffc/tnmgkw8jPyY/8/aIAR9lrRwosT5+wHwTHzriAZKoqhq+SmseywUW12SXLvMeKL/2+zyoFe6jEePWVX2fHoBloYQEnxcmM2ET2ITq1H+ag3HVAZjwV7fazEs2zHBLYl/3OdfHg/EBlPTimfoWAVzvmPxl1mYGmnJQPnLMJre8I5kC1lJ2RkyF6ekKubrD9s6cq4iedjJ99ha+Tn3wwQKjmeRJYWUkq+cutRT0i3ZeSXLLc8KHPOWRu//Pc+Yy0GZM7kG69uRnEeuwVcXQN4HZ9NBWR1nmhyc83KuLIj7vH9z4N29hWLg8FVQAffFSUGPZ63e57fBuIdbsAAvVuBlN8P1PPryASe6wbtwv8svzJ0lv9hLNcneawkY+FlSmYRLyVrJ+8LCbTKwjc9tiZfWdrjUTuqWCPGClT0KyS/aDlqvwto0Etys/JCLBOktK/bwVn2ez/5NXyL+A6XXTdjLZ/JAFdO66uhlLalR57PVoYY8n164ikgCx+LW1bMe8puXlgLHt1GfCz0eHJVFSYBLxtPsSDbnhbkKz8/0h6i/ejxbHPU/COh99L62rHHeGD4+MMi4IbN6yuAzFyHvfCPZkG5TfnRzqTM2RyXuZpqZ7ULe0CXTjbQ6x01z4rqJZs8leB5mWGP5VwLjjl/Bnr2JSE5wdey9PK6XXVpVV5A72ee32qUHzY5lr+rrWxJzYNkVwjiBw/xeZsmSOpE2fGdIr63hk0P1y/pzMQeUPV3IRs9DwgZSILnk9xqILM2Ab3QCfT05sA1gf1G5frdrelZWBONbPIHtKAe4OOk06FPwdGwXSwFSmiLczG7ul8Qz1ao9JvGmfZkEz/uns+9ny2QEPofssaWTRcn5Q/x2g9bEiRmKsTv+aRwKZDi9fj/ToLW+1QgpdR7vFtZtKcpJb4HEwDq/jqW93cZF0qMrUBkNZ3iFWIxXP9/UKPRCGy5pxXyc+YJ0tl6LLYU6PSLQC8dCbcykQn/mkmzJmvMAF31ZSDmcdaakCI8d8ZtAChDZNrPxGpEtgyU5fJ88xvjqvncYJx0HFjxVPbiOwADHYFBlvlqxHalqmNNSlUXJhDQAMzzn4Zoq875wra6ANG4J/73s3TUEFfEzAUmMREIH12NvlGp7mxPdXsn5gr07gYXiKWhZVGIg6jnI3/egXtG/KlxyTdgAkM90MiKcC3cCIkln40zHldq97nHNdkDG0A9+GMmQ2yWcPMPIN/r9/Q2pW7/hPjqF7t9304BnwHBvnNHfD9DpC87Yl9qwbZunj4udrtBQkJCQkJCQkJCQkJCQkJCQkJCQkJCwp74XoABAPzI5ErVaIx3AAAAAElFTkSuQmCC" />
                                                                </defs>
                                                            </svg>
                                                            {{ number_format($cashout->amount / (1 - ($adminCharge->value  / 100)),2) }}
                                                        </td>
                                                        <td>{{number_format($cashout->amount,2)}}</td>
                                                        @if($cashout->type == 'airtime')
                                                            <td class="type-airtime">Airtime</td>
                                                        @endif
                                                        @if($cashout->type == 'transfer')
                                                            <td class="type-transfer">Transfer</td>
                                                        @endif
                                                        <td>{{$cashout->status}}</td>
                                                        <td></td>
                                                    </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        {{--<span class="download-cashout-report">PDF--}}
                                          {{--<svg width="12" height="12" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
                                            {{--<path fill-rule="evenodd" clip-rule="evenodd" d="M0.923077 14H11.0769C11.5493 14 12 14.4507 12 15C12 15.5493 11.5493 16 11.0769 16H0.923077C0.450667 16 0 15.5493 0 15C0 14.4507 0.450667 14 0.923077 14Z"--}}
                                                  {{--fill="#6F6DCC" />--}}
                                            {{--<path fill-rule="evenodd" clip-rule="evenodd" d="M0.324244 7.04807L5.24259 11.6956C5.29629 11.746 5.35106 11.7725 5.40476 11.8233C5.43142 11.8485 5.45809 11.8485 5.4862 11.8737C5.51287 11.8989 5.56801 11.8989 5.59467 11.9244C5.62134 11.9496 5.67648 11.9496 5.70314 11.9496C5.72981 11.9496 5.75648 11.9748 5.81162 11.9748C5.86495 11.9748 5.94676 12 6.00009 12C6.00009 12 6.00009 12 6.02676 12C6.08045 12 6.1619 12 6.21559 11.9748C6.24226 11.9748 6.26893 11.9496 6.29704 11.9496C6.3237 11.9496 6.37884 11.9244 6.40551 11.9244C6.43218 11.8989 6.48731 11.8989 6.51398 11.8737C6.54065 11.8485 6.56731 11.8485 6.59542 11.8233C6.64912 11.7977 6.7039 11.746 6.75759 11.6956C6.75759 11.6956 6.75759 11.6956 6.78426 11.6956L11.6759 7.04807C12.108 6.6398 12.108 6.00136 11.6759 5.5931C11.459 5.38812 11.1887 5.28699 10.9184 5.28699C10.6482 5.28699 10.3779 5.38948 10.1613 5.5931L7.0812 8.50474V1.02151C7.0812 0.46036 6.59398 0 6.00009 0C5.4062 0 4.91898 0.46036 4.91898 1.02151V8.50474L1.83744 5.5931C1.62086 5.38812 1.35058 5.28699 1.0803 5.28699C0.810025 5.28699 0.539746 5.38948 0.322803 5.5931C-0.107841 6.00136 -0.107841 6.6398 0.324244 7.04807Z"--}}
                                                  {{--fill="#6F6DCC" />--}}
                                          {{--</svg>--}}
                                        {{--</span>--}}
                                    </div>
                                </div>
                            </div>

                            <div class="manage-users-section">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 manage-users">
                                            RECENT USERS
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Registration Date</th>
                                                        <th scope="col">Username</th>
                                                        <th scope="col">Name of User</th>
                                                        <th scope="col">Phone</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Email Verification</th>
                                                        <th scope="col">Points</th>
                                                        <th scope="col">Amount (₦)</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col">Ban/Unban User</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach($users as $user)
                                                        <tr>
                                                            <td>{{$user->created_at->diffForHumans()}}</td>
                                                            <td>{{$user->username}}</td>
                                                            <td>{{$user->name}}</td>
                                                            <td>{{$user->phone}}</td>
                                                            <td>{{$user->email}}</td>
                                                            <td>
                                                                @if($user->isEmailVerified)
                                                                    <span class="badge badge-success">Verified</span>
                                                                @else
                                                                    <span class="badge badge-danger">Not Verified</span>
                                                                @endif
                                                            </td>

                                                            <td>
                                                                <svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                    <rect width="23" height="18" fill="url(#pattern0)" />
                                                                    <defs>
                                                                        <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                                                            <use xlink:href="#image0" transform="translate(0 -0.00438596) scale(0.0105263 0.0134503)" />
                                                                        </pattern>
                                                                        <image id="image0" width="95" height="75" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF8AAABLCAYAAAAWC1t4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACRFJREFUeNrsXGtwFFUWPncm6SQmgRCEPOSRRCCskifJbunWyqR0sVakjFVWAbrlQgkra1mFaNU+fkjU3Sp3f7gmP9Z1cbeIrkqxVRb44pEAmYCKW+o6EEoXBcRHTAAhA5mANklfz713kpnu6ZnpGWcm6cn9qjo96e6k+37n3O+ce/reAZCQkJCQkJCQkJCQkJCQSAcQOz+8eujGWtwVAMFmEH9TAp89SuMBryQ/UWS/9ZNmJHYJbi78tdaEcONnL24e/PQq7t1KfZdHkh8L4Qcay5C4DbitNvXyyOQbr/Xgvg23HUrtPq8kPxzp3Q2M9Bb8uDoKodbJD+4RAMwIrUrNXq8kP5h49+LHOPEWCCX5daIVQceoehr/Sb8VQ5zC/RqlutM96clXu+qZjm8Z03MjWRl5QAqXAEypE6RnFQNcOg4w4tO3KGcegDMPwHcY6OUTQAc6AL49Ean3tKIBNk5a8tX9dc2ceEIKQgjKLgUy+z4k/iaA7/qAnt2FpH8KdNATuM5IaEY+GgiTobwaINOWooGGgJ75tzBEmOwI903Koj3eSUU+Es+C6RZTAuesBVKyAonbCfSrf4WXkyjHSOGtwgiZRUD7n0XDHTLrBSwbSpkBxp18dV/tar/U6MkqWAxk/qMoF32gHf8T93irREc6RqbciMZcDxRliPb+FUAbMsuImpTrd3vTmnwkng2SPgwhqGgZkPKNQL/4J9C+bXETHfYYxgNyzSNAsitA+/IJNOxnxuxJ9IAkG2DcyFf31jBt/4xrfHD2smATkOlLQDvyG67rP5joSFI0/U4gV98D2ue/QwOcNF7brly3a00yOXCMo+Nv4YOmYE+Yu85P/HqAoU+S/gD0/A6MJZvBMfcvGNQrjKdXqx/9ojntyFc7q1mjdA0j011ASleCdhiJ95kQj8HXOWcdZDRsF2lkogxwYS/Qcy+DYxbGF2duiIOoH99WkDbkI/GsMU/rDmaXAKncBPTY42E9PrNxBzhY5pNVAk40UkJ7wMCrQFH3SUlIqs+etSWdPJ+llWW6h1jQgunf60C/CT/Q1M51B67H1DOR3s8N0N+Khr0WyNRbjKceQu8vSwYRGSmnntINY4HNn1JC3gKgRx+J+GfaF8+Bo+h2/1PngRMHXdr5g0GlBeJPH0jYY3ToSIQb4CDs7HNAZqwDenGfIS0hG/BnwkfAKc121I4qpvPbgzMOR+1mbHQX5txbo2YnzsoWcMxcFp/NcXA2/PEvo97DMftJEQdGDSDOeZWFb06zu+zcofstr1J4PUqOteD4Qdw31vpfsHYP72sY/O8O0X71/8ua7U6+PsOZtQoHUUj88KC1h8VsJy6vv3wCJWqPtWt97wo9yKqI7Dh2Il/ds8gVktej3tP+16zpI/YQlunEg5Hev8VmLN9/gUy52XjYZeeA6zKmlyx3h8FjEByAw8KZH0rSxQ/5FohchqDLrmHVzNHqp1Vc7gFSuBKo/miZeuz2AqXyDa8dyZ+r9/oGHEwds54Z5M3Xa3jfNhg51Wa9vBCTTB3FXlZhdorVotx21Pwyo+fTgRgC6LBPb4zcBcl7UlbpHD5jpvu19s7zg8oFcGXQujcaRr5kSh1k3vBO1PSSv8nCYKtdeDu257tyBogz1yg9CS01pMzzSWGjSy8jmGYOvB9DEPwEKKvpx3JPpRgcU38KzvInwFn8K5hocICNMPLRb0Pkx3JDi+9NeEnCtrJDv/0aIB+93/t+TNIzfPQBcJSuAMKyJeMAfTTTySrmXh9iAOwFGnuHO9nIp+ffc/tnmgkw8jPyY/8/aIAR9lrRwosT5+wHwTHzriAZKoqhq+SmseywUW12SXLvMeKL/2+zyoFe6jEePWVX2fHoBloYQEnxcmM2ET2ITq1H+ag3HVAZjwV7fazEs2zHBLYl/3OdfHg/EBlPTimfoWAVzvmPxl1mYGmnJQPnLMJre8I5kC1lJ2RkyF6ekKubrD9s6cq4iedjJ99ha+Tn3wwQKjmeRJYWUkq+cutRT0i3ZeSXLLc8KHPOWRu//Pc+Yy0GZM7kG69uRnEeuwVcXQN4HZ9NBWR1nmhyc83KuLIj7vH9z4N29hWLg8FVQAffFSUGPZ63e57fBuIdbsAAvVuBlN8P1PPryASe6wbtwv8svzJ0lv9hLNcneawkY+FlSmYRLyVrJ+8LCbTKwjc9tiZfWdrjUTuqWCPGClT0KyS/aDlqvwto0Etys/JCLBOktK/bwVn2ez/5NXyL+A6XXTdjLZ/JAFdO66uhlLalR57PVoYY8n164ikgCx+LW1bMe8puXlgLHt1GfCz0eHJVFSYBLxtPsSDbnhbkKz8/0h6i/ejxbHPU/COh99L62rHHeGD4+MMi4IbN6yuAzFyHvfCPZkG5TfnRzqTM2RyXuZpqZ7ULe0CXTjbQ6x01z4rqJZs8leB5mWGP5VwLjjl/Bnr2JSE5wdey9PK6XXVpVV5A72ee32qUHzY5lr+rrWxJzYNkVwjiBw/xeZsmSOpE2fGdIr63hk0P1y/pzMQeUPV3IRs9DwgZSILnk9xqILM2Ab3QCfT05sA1gf1G5frdrelZWBONbPIHtKAe4OOk06FPwdGwXSwFSmiLczG7ul8Qz1ao9JvGmfZkEz/uns+9ny2QEPofssaWTRcn5Q/x2g9bEiRmKsTv+aRwKZDi9fj/ToLW+1QgpdR7vFtZtKcpJb4HEwDq/jqW93cZF0qMrUBkNZ3iFWIxXP9/UKPRCGy5pxXyc+YJ0tl6LLYU6PSLQC8dCbcykQn/mkmzJmvMAF31ZSDmcdaakCI8d8ZtAChDZNrPxGpEtgyU5fJ88xvjqvncYJx0HFjxVPbiOwADHYFBlvlqxHalqmNNSlUXJhDQAMzzn4Zoq875wra6ANG4J/73s3TUEFfEzAUmMREIH12NvlGp7mxPdXsn5gr07gYXiKWhZVGIg6jnI3/egXtG/KlxyTdgAkM90MiKcC3cCIkln40zHldq97nHNdkDG0A9+GMmQ2yWcPMPIN/r9/Q2pW7/hPjqF7t9304BnwHBvnNHfD9DpC87Yl9qwbZunj4udrtBQkJCQkJCQkJCQkJCQkJCQkJCQkJCwp74XoABAPzI5ErVaIx3AAAAAElFTkSuQmCC" />
                                                                    </defs>
                                                                </svg>
                                                                {{$user->points}}
                                                            </td>
                                                            <td>{{$user->points * (1 - ($adminCharge->value  / 100)) }}</td>


                                                            <td>
                                                                @if($user->isBanned == 1)
                                                                    <span style="color:red">Banned</span>
                                                                @else
                                                                    <span style="color:green">Active</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($user->isBanned == 1)
                                                                    <a href="{{url('user/' . $user->uid . '/unban')}}">
                                                                        <span class="urban">Unban</span>
                                                                    </a>
                                                                @else
                                                                    <a href="{{url('user/' . $user->uid . '/ban')}}">
                                                                        <span class="ban">Ban</span>
                                                                    </a>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 cash-out">
                                            RECENT DEPOSITS
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">Reference</th>
                                                        <th scope="col">User</th>
                                                        <th scope="col">Points</th>
                                                        <th scope="col">Amount(&#x20A6;)</th>
                                                        <th scope="col">Charge(&#x20A6;)</th>
                                                        <th scope="col">Type</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($payments as $payment)
                                                        <tr>
                                                            <td>{{$payment->created_at->diffForHumans()}}</td>
                                                            <td>{{$payment->reference}}</td>
                                                            <td>{{$payment->User->username}}</td>
                                                            <td>
                                                                <svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                    <rect width="23" height="18" fill="url(#pattern0)" />
                                                                    <defs>
                                                                        <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                                                            <use xlink:href="#image0" transform="translate(0 -0.00438596) scale(0.0105263 0.0134503)" />
                                                                        </pattern>
                                                                        <image id="image0" width="95" height="75" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF8AAABLCAYAAAAWC1t4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACRFJREFUeNrsXGtwFFUWPncm6SQmgRCEPOSRRCCskifJbunWyqR0sVakjFVWAbrlQgkra1mFaNU+fkjU3Sp3f7gmP9Z1cbeIrkqxVRb44pEAmYCKW+o6EEoXBcRHTAAhA5mANklfz713kpnu6ZnpGWcm6cn9qjo96e6k+37n3O+ce/reAZCQkJCQkJCQkJCQkJCQSAcQOz+8eujGWtwVAMFmEH9TAp89SuMBryQ/UWS/9ZNmJHYJbi78tdaEcONnL24e/PQq7t1KfZdHkh8L4Qcay5C4DbitNvXyyOQbr/Xgvg23HUrtPq8kPxzp3Q2M9Bb8uDoKodbJD+4RAMwIrUrNXq8kP5h49+LHOPEWCCX5daIVQceoehr/Sb8VQ5zC/RqlutM96clXu+qZjm8Z03MjWRl5QAqXAEypE6RnFQNcOg4w4tO3KGcegDMPwHcY6OUTQAc6AL49Ean3tKIBNk5a8tX9dc2ceEIKQgjKLgUy+z4k/iaA7/qAnt2FpH8KdNATuM5IaEY+GgiTobwaINOWooGGgJ75tzBEmOwI903Koj3eSUU+Es+C6RZTAuesBVKyAonbCfSrf4WXkyjHSOGtwgiZRUD7n0XDHTLrBSwbSpkBxp18dV/tar/U6MkqWAxk/qMoF32gHf8T93irREc6RqbciMZcDxRliPb+FUAbMsuImpTrd3vTmnwkng2SPgwhqGgZkPKNQL/4J9C+bXETHfYYxgNyzSNAsitA+/IJNOxnxuxJ9IAkG2DcyFf31jBt/4xrfHD2smATkOlLQDvyG67rP5joSFI0/U4gV98D2ue/QwOcNF7brly3a00yOXCMo+Nv4YOmYE+Yu85P/HqAoU+S/gD0/A6MJZvBMfcvGNQrjKdXqx/9ojntyFc7q1mjdA0j011ASleCdhiJ95kQj8HXOWcdZDRsF2lkogxwYS/Qcy+DYxbGF2duiIOoH99WkDbkI/GsMU/rDmaXAKncBPTY42E9PrNxBzhY5pNVAk40UkJ7wMCrQFH3SUlIqs+etSWdPJ+llWW6h1jQgunf60C/CT/Q1M51B67H1DOR3s8N0N+Khr0WyNRbjKceQu8vSwYRGSmnntINY4HNn1JC3gKgRx+J+GfaF8+Bo+h2/1PngRMHXdr5g0GlBeJPH0jYY3ToSIQb4CDs7HNAZqwDenGfIS0hG/BnwkfAKc121I4qpvPbgzMOR+1mbHQX5txbo2YnzsoWcMxcFp/NcXA2/PEvo97DMftJEQdGDSDOeZWFb06zu+zcofstr1J4PUqOteD4Qdw31vpfsHYP72sY/O8O0X71/8ua7U6+PsOZtQoHUUj88KC1h8VsJy6vv3wCJWqPtWt97wo9yKqI7Dh2Il/ds8gVktej3tP+16zpI/YQlunEg5Hev8VmLN9/gUy52XjYZeeA6zKmlyx3h8FjEByAw8KZH0rSxQ/5FohchqDLrmHVzNHqp1Vc7gFSuBKo/miZeuz2AqXyDa8dyZ+r9/oGHEwds54Z5M3Xa3jfNhg51Wa9vBCTTB3FXlZhdorVotx21Pwyo+fTgRgC6LBPb4zcBcl7UlbpHD5jpvu19s7zg8oFcGXQujcaRr5kSh1k3vBO1PSSv8nCYKtdeDu257tyBogz1yg9CS01pMzzSWGjSy8jmGYOvB9DEPwEKKvpx3JPpRgcU38KzvInwFn8K5hocICNMPLRb0Pkx3JDi+9NeEnCtrJDv/0aIB+93/t+TNIzfPQBcJSuAMKyJeMAfTTTySrmXh9iAOwFGnuHO9nIp+ffc/tnmgkw8jPyY/8/aIAR9lrRwosT5+wHwTHzriAZKoqhq+SmseywUW12SXLvMeKL/2+zyoFe6jEePWVX2fHoBloYQEnxcmM2ET2ITq1H+ag3HVAZjwV7fazEs2zHBLYl/3OdfHg/EBlPTimfoWAVzvmPxl1mYGmnJQPnLMJre8I5kC1lJ2RkyF6ekKubrD9s6cq4iedjJ99ha+Tn3wwQKjmeRJYWUkq+cutRT0i3ZeSXLLc8KHPOWRu//Pc+Yy0GZM7kG69uRnEeuwVcXQN4HZ9NBWR1nmhyc83KuLIj7vH9z4N29hWLg8FVQAffFSUGPZ63e57fBuIdbsAAvVuBlN8P1PPryASe6wbtwv8svzJ0lv9hLNcneawkY+FlSmYRLyVrJ+8LCbTKwjc9tiZfWdrjUTuqWCPGClT0KyS/aDlqvwto0Etys/JCLBOktK/bwVn2ez/5NXyL+A6XXTdjLZ/JAFdO66uhlLalR57PVoYY8n164ikgCx+LW1bMe8puXlgLHt1GfCz0eHJVFSYBLxtPsSDbnhbkKz8/0h6i/ejxbHPU/COh99L62rHHeGD4+MMi4IbN6yuAzFyHvfCPZkG5TfnRzqTM2RyXuZpqZ7ULe0CXTjbQ6x01z4rqJZs8leB5mWGP5VwLjjl/Bnr2JSE5wdey9PK6XXVpVV5A72ee32qUHzY5lr+rrWxJzYNkVwjiBw/xeZsmSOpE2fGdIr63hk0P1y/pzMQeUPV3IRs9DwgZSILnk9xqILM2Ab3QCfT05sA1gf1G5frdrelZWBONbPIHtKAe4OOk06FPwdGwXSwFSmiLczG7ul8Qz1ao9JvGmfZkEz/uns+9ny2QEPofssaWTRcn5Q/x2g9bEiRmKsTv+aRwKZDi9fj/ToLW+1QgpdR7vFtZtKcpJb4HEwDq/jqW93cZF0qMrUBkNZ3iFWIxXP9/UKPRCGy5pxXyc+YJ0tl6LLYU6PSLQC8dCbcykQn/mkmzJmvMAF31ZSDmcdaakCI8d8ZtAChDZNrPxGpEtgyU5fJ88xvjqvncYJx0HFjxVPbiOwADHYFBlvlqxHalqmNNSlUXJhDQAMzzn4Zoq875wra6ANG4J/73s3TUEFfEzAUmMREIH12NvlGp7mxPdXsn5gr07gYXiKWhZVGIg6jnI3/egXtG/KlxyTdgAkM90MiKcC3cCIkln40zHldq97nHNdkDG0A9+GMmQ2yWcPMPIN/r9/Q2pW7/hPjqF7t9304BnwHBvnNHfD9DpC87Yl9qwbZunj4udrtBQkJCQkJCQkJCQkJCQkJCQkJCQkJCwp74XoABAPzI5ErVaIx3AAAAAElFTkSuQmCC" />
                                                                    </defs>
                                                                </svg>
                                                                {{ number_format($payment->amount,2) }}
                                                            </td>
                                                            <td>{{number_format($payment->amount,2)}}</td>
                                                            <td>{{ ($payment->amount * 0.0148)}}</td>
                                                            <td>{{$payment->type}}</td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        {{--<div class="col-lg-5 col-md-12 col-sm-12 cash-out">--}}
                                            {{--RECENT DEPOSITS--}}
                                            {{--<div class="table-responsive">--}}
                                                {{--<table class="table">--}}
                                                    {{--<thead>--}}
                                                    {{--<tr>--}}
                                                        {{--<th scope="col">Name</th>--}}
                                                        {{--<th scope="col">Value</th>--}}
                                                        {{--<th scope="col">Date Changed</th>--}}
                                                        {{--<th scope="col">Edit</th>--}}
                                                    {{--</tr>--}}
                                                    {{--</thead>--}}
                                                    {{--<tbody>--}}
                                                    {{--<tr>--}}
                                                        {{--<td>--}}
                                                            {{--Administrative Charge--}}
                                                        {{--</td>--}}
                                                        {{--<td>{{$adminCharge->value}}%</td>--}}
                                                        {{--<td>--}}
                                                            {{--{{$adminCharge->created_at->diffForHumans()}}--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<a href="{{url('settings')}}">--}}
                                                                {{--<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
                                                                    {{--<path d="M14.4647 13.6047H0.522515C0.229397 13.6047 0 13.8472 0 14.1279C0 14.4087 0.242141 14.6512 0.522515 14.6512H14.4775C14.7706 14.6512 15 14.4087 15 14.1279C15 13.8472 14.7579 13.6047 14.4647 13.6047Z"--}}
                                                                          {{--fill="#979797" />--}}
                                                                    {{--<path d="M0.0125375 8.89535L0 11.335C0 11.4726 0.05015 11.6102 0.15045 11.7103C0.25075 11.8104 0.376125 11.8605 0.514037 11.8605L2.94631 11.848C3.08422 11.848 3.2096 11.7979 3.3099 11.6978L11.71 3.31543C11.9106 3.11525 11.9106 2.78996 11.71 2.57727L9.30282 0.150132C9.10222 -0.0500442 8.77624 -0.0500442 8.56311 0.150132L6.88308 1.83912L0.162987 8.53253C0.075225 8.63262 0.0125375 8.75773 0.0125375 8.89535ZM8.93923 1.2511L10.6318 2.94009L9.67894 3.89093L7.98638 2.20194L8.93923 1.2511ZM1.05315 9.12055L7.24667 2.94009L8.93923 4.62909L2.74571 10.797L1.04061 10.8095L1.05315 9.12055Z"--}}
                                                                          {{--fill="#979797" />--}}
                                                                {{--</svg>--}}
                                                            {{--</a>--}}

                                                        {{--</td>--}}

                                                    {{--</tr>--}}
                                                    {{--</tbody>--}}
                                                {{--</table>--}}
                                            {{--</div>--}}
                                            {{--Revenue Count--}}
                                            {{--<div class="container revenue-count pt-3">--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-lg-3 col-md-6 col-sm-6">--}}
                                                        {{--<div class="revenue-day">--}}
                                                            {{--<p>Day</p>--}}
                                                            {{--<p>34,500</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-lg-3 col-md-6 col-sm-6">--}}
                                                        {{--<div class="revenue-week">--}}
                                                            {{--<p>Week</p>--}}
                                                            {{--<p>34,500</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-lg-3 col-md-6 col-sm-6">--}}
                                                        {{--<div class="revenue-month">--}}
                                                            {{--<p>Month</p>--}}
                                                            {{--<p>34,500</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-lg-3 col-md-6 col-sm-6">--}}
                                                        {{--<div class="revenue-alltime">--}}
                                                            {{--<p>Alltime</p>--}}
                                                            {{--<p>34,500</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Page Main Content-->
@endsection
