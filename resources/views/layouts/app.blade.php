<!doctype html>
<html lang="en">

<head>
    <title>Strom Web</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{url('img/strom.png')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
          crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/style.css')}}">

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"--}}
            {{--crossorigin="anonymous"></script>--}}

    <link rel="stylesheet" href="{{url('css/jquery-ui.min.css')}}">
    <link href="{{url('css/jquery-ui-timepicker-addon.min.css')}}" rel="stylesheet">

    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/jquery-ui.min.js')}}"></script>

    <script src="{{url('js/jquery-ui-timepicker-addon.min.js')}}"></script>


    <!-- Font Awesome JS-->
    <script defer src="https://use.fontawesome.com/releases/v5.4.1/js/all.js" integrity="sha384-L469/ELG4Bg9sDQbl0hvjMq8pOcqFgkSpwhwnslzvVVGpDjYJ6wJJyYjvG3u8XW7"
            crossorigin="anonymous"></script>
</head>

<body>
<style>
    .badge{
        padding: 5px 10px !important;
    }
</style>
<div class="wrapper">

    @include('navs.left')
    @yield('content')
</div>

<!-- Optional JavaScript -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>

<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('select').select2();
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
        $('#dashboardButton').on('click', function () {
            $('#sidebar').removeClass('active');
        });
        $('#taskButton').on('click', function () {
            $('#sidebar').removeClass('active');
        });
        $('#userAccountButton').on('click', function () {
            $('#sidebar').removeClass('active');
        });
        $('#cashoutButton').on('click', function () {
            $('#sidebar').removeClass('active');
        });
        $('#settingsButton').on('click', function () {
            $('#sidebar').removeClass('active');
        });
    });
</script>
</body>

</html>
