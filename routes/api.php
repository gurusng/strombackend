<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('sign-up','ApiController@signUp');
Route::post('sign-in','ApiController@signIn');
Route::post('verify-code','ApiController@verifyCode');
Route::post('verify-email-code','ApiController@verifyEmailCode');

Route::post('reset-password','ApiController@resetPassword');
Route::post('reset-password-code','ApiController@resetPasswordCode');

Route::post('resend-code','ApiController@resendCode');
Route::post('edit-profile','ApiController@editProfile');
Route::post('change-password','ApiController@changePassword');

Route::post('tasks','ApiController@tasks');
Route::post('pending-tasks','ApiController@pendingTasks');

Route::post('search-videos','ApiController@searchVideos');


Route::post('task-details','ApiController@taskDetails');
Route::post('user-tasks','ApiController@userTasks');

Route::post('user-ads','ApiController@userAds');
Route::post('user-ad-details','ApiController@userAdDetails');
Route::post('user-ad-details-approval','ApiController@userAdDetailsApproval');
Route::post('user-ad-details-verification','ApiController@userAdDetailsVerification');

Route::post('user-updated-details','ApiController@userUpdatedDetails');
Route::post('change-profile-photo','ApiController@changeProfilePhoto');

Route::post('resolve-account-name','ApiController@resolveAccountName');
Route::post('add-card','ApiController@addCard');
Route::post('delete-card','ApiController@deleteCard');
Route::post('authorize-payment','ApiController@authorizePayment');

Route::post('pay','ApiController@pay');
Route::post('web-pay','ApiController@webPay');
Route::post('credit-payments','ApiController@creditPayments');

Route::post('start-task','ApiController@startTask');
Route::post('approve-task','ApiController@approveTask');
Route::post('approve-all-tasks','ApiController@approveAllTasks');
Route::post('complete-task','ApiController@completeTask');
Route::post('verify-task','ApiController@verifyTask');
Route::post('verify-all-tasks','ApiController@verifyAllTasks');
Route::post('reject-task','ApiController@rejectTask');


Route::post('add-social-task','ApiController@addSocialTask');
Route::post('add-video-task','ApiController@addVideoTask');
Route::post('add-community-task','ApiController@addCommunityTask');
Route::post('add-community-task-image','ApiController@addCommunityTaskImage');


Route::post('my-referrals','ApiController@myReferrals');

Route::post('play-video','ApiController@playVideo');
Route::post('add-rating','ApiController@addRating');

Route::post('cash-out-transfer','ApiController@cashOutTransfer');
Route::post('cash-out-airtime','ApiController@cashOutAirtime');

Route::post('test-notification','ApiController@testNotification');
