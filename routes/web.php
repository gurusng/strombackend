<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', 'PublicController@index');

Route::get('confirm-email/{code}/{uid}','PublicController@confirmEmail');

Route::get('privacy-policy','PublicController@privacyPolicy');
Route::get('terms','PublicController@terms');


Auth::routes();

Route::get('home',function(){return redirect('/dashboard');});
Route::get('/dashboard', 'UserController@index')->name('dashboard');

//admin role
Route::get('admin/dashboard','AdminController@dashboard');

Route::get('users','AdminController@users');
Route::get('users/banned','AdminController@bannedUsers');
Route::get('user/{uid}','AdminController@userDetails');
Route::get('user/{uid}/edit','AdminController@editUser');
Route::get('user/{uid}/ban','AdminController@banUser');
Route::get('user/{uid}/unban','AdminController@unbanUser');


Route::get('tasks','AdminController@tasks')->name('tasks.show');
Route::get('task/{task}','AdminController@taskDetails')->name('tasks.details');
Route::get('task/{task}/delete','AdminController@deleteTask')->name('tasks.destroy');
Route::get('tasks/pending','AdminController@pendingVideos')->name('tasks.pending');
Route::get('task/{task}/approve','AdminController@approveVideo')->name('tasks.approve');
Route::get('task/{task}/reject','AdminController@rejectVideo')->name('tasks.reject');


Route::get('cashouts','AdminController@cashouts')->name('cashouts.show');
Route::get('cashout/{cashout}/approve','AdminController@approveCashout')->name('cashouts.approve');
Route::get('cashout/{cashout}/reject','AdminController@rejectCashout')->name('cashouts.reject');


Route::get('deposits','AdminController@deposits')->name('deposits.show');

Route::get('summary','AdminController@summary')->name('summary.show');

Route::get('settings','AdminController@settings');
Route::get('settings/add','AdminController@addSetting');
Route::post('settings/add','AdminController@postAddSetting');
Route::get('settings/resend-unverified-emails','AdminController@resendUnverifiedEmails')->name('settings.resendUnverifiedEmails');


Route::get('task/add','UserController@addTask');

Route::get('task/{tid}/suspend','UserController@suspendTask');
Route::get('task/{tid}/resume','UserController@resumeTask');


Route::post('task/add','UserController@postAddTask');


//funding
Route::get('fund-account','AdminController@fundAccount')->name('funds.create');
Route::post('fund-account','AdminController@postFundAccount')->name('funds.store');

Route::get('logout','HomeController@logout');
