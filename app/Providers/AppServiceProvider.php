<?php

namespace App\Providers;

use App\setting;
use App\task;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $adminCharge = $this->getAdminCharge();

        $notifications = task::whereIn('type',['videov2e','videop2v'])->where('isSuspended',1)->count();
        view()->share('adminCharge',$adminCharge);
        view()->share('notifications',$notifications);

        Bugsnag::setHostname(env('APP_URL'));


        Bugsnag::registerCallback(function ($report) {

            $report->setMetaData([
                'environment' => env('APP_ENV'),
                'url' => env('APP_URL')
            ]);

        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

	public function getAdminCharge() {
        try{


		if(setting::where('name','adminCharge')->count() <= 0) {
			$setting = new setting();
			$setting->uid = 1;
			$setting->name = 'adminCharge';
			$setting->value = 17.5;
			$setting->save();
		}
		$adminCharge = setting::where('name','adminCharge')->get()->last();

		return $adminCharge;
        }catch (\Exception $exception){
            $setting = new setting();
            $setting->uid = 1;
            $setting->name = 'adminCharge';
            $setting->value = 17.5;
            return $setting;
        }

	}

}
