<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rating extends Model
{
    protected $primaryKey = 'ratid';
    protected $table = 'ratings';


	public function Media() {
		return $this->belongsTo(taskMedia::class,'tmid','tmid');
    }

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }

}
