<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'uid';

    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];


	public function Payments() {
		return $this->hasMany(payment::class,'uid','uid');
    }

	public function Cards() {
		return $this->hasMany(card::class,'uid','uid');
    }

	public function Cashouts() {
		return $this->hasMany(cashout::class,'uid','uid');
    }

	public function Ads() {
		return $this->hasMany(task::class,'uid','uid');
    }

	public function Tasks() {
		return $this->belongsToMany(task::class,'task_users','uid','tid');
    }



}
