<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class card extends Model
{
	protected $primaryKey = 'cardid';
	protected $table = 'cards';
	protected $hidden = ['authCode'];

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
	}
}
