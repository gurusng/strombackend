<?php

namespace App\Http\Controllers;

use App\cashout;
use App\helpers\GuruHelpers;
use App\payment;
use App\setting;
use App\task;
use App\taskMedia;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class AdminController extends Controller
{

	public function __construct() {
		$this->middleware('auth');
	}

    public function resendUnverifiedEmails() {


	    Artisan::queue("resend:emails");
        session()->flash('success','Email sending started.');

        return redirect()->back();
	}

	public function dashboard() {
		$users = User::all()->sortByDesc('created_at')->take(5);
		$tasks = task::all()->sortByDesc('created_at')->take(5);
		$cashouts = cashout::all()->sortByDesc('created_at')->take(5);
		$payments = payment::all()->sortByDesc('created_at')->take(5);

		//get user info
		$allUsers = User::all();
		$usersToday = User::where('created_at','>',Carbon::today())->get();
		$usersThisWeek = User::where('created_at','>',Carbon::now()->startOfWeek())->get();
		$usersThisMonth = User::where('created_at','>',Carbon::now()->startOfMonth())->get();

		//get task info

        $allTasks = task::all();
        $tasksToday = task::where('created_at','>',Carbon::today())->get();
        $tasksThisWeek = task::where('created_at','>',Carbon::now()->startOfWeek())->get();
        $tasksThisMonth = task::where('created_at','>',Carbon::now()->startOfMonth())->get();

        // get payment info

        $allPayments = payment::all()->where('status','paid')->sum('amount');
        $paymentsToday = payment::where('created_at','>',Carbon::today())->where('status','paid')->sum('amount');
        $paymentsThisWeek = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfWeek())->sum('amount');
        $paymentsThisMonth = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfMonth())->sum('amount');


        //get cashout info

        $allCashouts = cashout::all()->where('status','approved')->sum('amount');
        $cashoutsToday = cashout::where('status','approved')->where('created_at','>',Carbon::today())->sum('amount');
        $cashoutsThisWeek = cashout::where('status','approved')->where('created_at','>',Carbon::now()->startOfWeek())->sum('amount');
        $cashoutsThisMonth = cashout::where('status','approved')->where('created_at','>',Carbon::now()->startOfMonth())->sum('amount');



		return view('dashboard',[
			'users' => $users,
			'tasks'  => $tasks,
            'cashouts' => $cashouts,
            'payments' => $payments,

            // user info

            'allUsers' => $allUsers,
            'usersToday' => $usersToday,
            'usersThisWeek' => $usersThisWeek,
            'usersThisMonth' => $usersThisMonth,

            //task info

            'allTasks' => $allTasks,
            'tasksToday' => $tasksToday,
            'tasksThisWeek' => $tasksThisWeek,
            'tasksThisMonth' => $tasksThisMonth,

            //payment info

            'allPayments' => $allPayments,
            'paymentsToday' => $paymentsToday,
            'paymentsThisWeek' => $paymentsThisWeek,
            'paymentsThisMonth' => $paymentsThisMonth,

            //cashout info

            'allCashouts' => $allCashouts,
            'cashoutsToday' => $cashoutsToday,
            'cashoutsThisWeek' => $cashoutsThisWeek,
            'cashoutsThisMonth' => $cashoutsThisMonth

		]);

    }

    public function cashouts() {
        if(Input::has('search')){
            $searchTerm = Input::get('search');
            $cashouts = cashout::where('reference','like',"%$searchTerm%")->where('accountNumber','like',"%$searchTerm%")->where('accountName','like',"%$searchTerm%")->paginate(20);

        } else if(Input::has('status')){
            $searchTerm = Input::get('search');
            $status = Input::get('status');
            $cashouts = cashout::where('status',$status)->where('reference','like',"%$searchTerm%")->where('accountNumber','like',"%$searchTerm%")->where('accountName','like',"%$searchTerm%")->paginate(20);

        } else

            $cashouts = cashout::paginate(20);

        return view('cashouts.manage',[
            'cashouts' => $cashouts
        ]);
    }

    public function pendingVideos() {
        $pendingVideos = task::whereIn('type',['videov2e','videop2v'])->where('isSuspended',1)->paginate(20);

        return view('tasks.pending',[
            'tasks' => $pendingVideos
        ]);
    }

    public function approveVideo(task $task) {

	    $task->isSuspended = 0;
	    $task->save();

	    GuruHelpers::sendNotificationTo($task->User->username,"Your task '$task->name' has been approved.");

	    session()->flash('success','Task Approved.');
	    return redirect()->route('tasks.pending');

    }

    public function rejectVideo(task $task){

	    GuruHelpers::sendNotificationTo($task->User->username, "Your task '$task->name' has been rejected. Please ensure it complies with our policy, if you need help, please contact us. Contact info is on help page.");

	    try{
            $this->deleteTask($task);

        }catch (\Exception $exception){
	        Bugsnag::notifyException($exception);
        }

        session()->flash('success','Task Rejected.');

	    return redirect()->route('tasks.pending');
    }

    public function approveCashout(cashout $cashout) {

	    DB::beginTransaction();
        $cashout->status = 'approved';
        $cashout->approvedBy = auth()->user()->uid;
        $cashout->save();

        $user = User::find($cashout->uid);
        $user->points = $user->points - $cashout->amount;
        $user->save();

        DB::commit();

        if($cashout->type == 'transfer'){
            GuruHelpers::makeSingleTransfer($cashout->bankCode,$cashout->accountNumber,$cashout->amount,"Strom Cashout $cashout->cashid", $cashout->reference);

        }

        if($cashout->type == 'airtime'){
            file_get_contents("https://mobileairtimeng.com/httpapi/?userid=08081234504&pass=2c77db87d9f34232a2844c&network=1&phone=$cashout->phone&amt=$cashout->amount&jsn=json");
        }


        session()->flash('success','Cashout Approved.');
        return redirect()->back();
    }

    public function rejectCashout(cashout $cashout) {
        $cashout->status = 'rejected';
        $cashout->approvedBy = auth()->user()->uid;
        $cashout->save();
        session()->flash('success','Cashout Rejected.');
        return redirect()->back();
    }

    public function summary() {



        $allPayments = payment::all()->where('status','paid')->sum('amount') * 0.175;
        $paymentsToday = payment::where('created_at','>',Carbon::today())->where('status','paid')->sum('amount') * 0.175;
        $paymentsThisWeek = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfWeek())->sum('amount') * 0.175;
        $paymentsThisMonth = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfMonth())->sum('amount') * 0.175;

        //get charge info
        $allCharges = payment::all()->where('status','paid')->sum('charge');
        $chargesToday = payment::where('created_at','>',Carbon::today())->where('status','paid')->sum('charge');
        $chargessThisWeek = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfWeek())->sum('charge');
        $chargessThisMonth = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfMonth())->sum('charge');



        return view('summary.manage',[
            //payment info

//            'balance' => $response,
            'allPayments' => $allPayments,
            'paymentsToday' => $paymentsToday,
            'paymentsThisWeek' => $paymentsThisWeek,
            'paymentsThisMonth' => $paymentsThisMonth,

            //charge info

            'allCharges' => $allCharges,
            'chargesToday' => $chargesToday,
            'chargesThisWeek' => $chargessThisWeek,
            'chargesThisMonth' => $chargessThisMonth

        ]);
    }

    public function deposits() {

        if(Input::has('search')){
            $searchTerm = Input::get('search');
            $payments = payment::orderBy('created_at','desc')->where('reference','like',"%$searchTerm%")->paginate(20);

        } else if(Input::has('status')){
            $searchTerm = Input::get('search');
            $status = Input::get('status');
            $payments = payment::orderBy('created_at','desc')->where('status',$status)->where('reference','like',"%$searchTerm%")->paginate(20);

        } else

            $payments = payment::orderBy('created_at','desc')->paginate(20);

        return view('deposits.manage',[
            'payments' => $payments
        ]);
    }

    public function tasks() {
	    if(Input::has('search')){
	        $searchTerm = Input::get('search');
            $type = Input::get('type');

            if(!empty($type) && $type == 'social')
            $tasks = task::orderBy('created_at','desc')->where('type',$type)->where('name','like',"%$searchTerm%")->orWhere('location','like',"%$searchTerm%")->where('type',$type)->paginate(20);

            else if($type == 'video')

            $tasks = task::orderBy('created_at','desc')->whereIn('type',['videov2e','videop2v'])->where('name','like',"%$searchTerm%")->paginate(20);
            else if($type == 'community')
            $tasks = task::orderBy('created_at','desc')->where('name','like',"%$searchTerm%")->orWhere('location','like',"%$searchTerm%")->paginate(20);
            else
                $tasks = task::orderBy('created_at','desc')->where('name','like',"%$searchTerm%")->paginate(20);

        } else if(Input::has('type')){
            $searchTerm = Input::get('search');
	        $type = Input::get('type');

	        if($type == 'video'){
                $tasks = task::orderBy('created_at','desc')->whereIn('type',['videov2e','videop2v'])->where('name','like',"%$searchTerm%")->paginate(20);
            } else

            if($type == 'community')

	        $tasks = task::orderBy('created_at','desc')->where('type',$type)->where('name','like',"%$searchTerm%")->orWhere('location','like',"%$searchTerm%")->paginate(20);
	        else
	        $tasks = task::orderBy('created_at','desc')->where('type',$type)->where('name','like',"%$searchTerm%")->paginate(20);


        } else

        $tasks = task::orderBy('created_at','desc')->paginate(20);
        return view('tasks.manage',[
            'tasks' => $tasks
        ]);
	}

    public function taskDetails(task $task) {
	    return view('tasks.details',[
	        'task' => $task
        ]);
	}

    public function deleteTask(task $task) {

	    //delete task and media files associated with it
	    foreach($task->Media as $media){

	        if($task->type == 'videov2e' || $task->type == 'videop2v') {
                $urlArray = explode("/", $media->url);
                $fileName = $urlArray[3] . "/" . $urlArray[4] . "/" . $urlArray[5];
                unlink($fileName);
            } else {
                $urlArray = explode("/", $media->url);
                $fileName = $urlArray[3] . "/" . $urlArray[4] . "/" . $urlArray[5] . "/" . $urlArray[6];
                unlink($fileName);
            }


            taskMedia::destroy($media->tmid);
        }

        task::destroy($task->tid);

	    session()->flash('success','Task Deleted.');
	    return redirect()->back();
	}

	public function getAdminCharge() {
		if(setting::where('name','adminCharge')->count() <= 0) {
			$setting = new setting();
			$setting->uid = auth()->user()->uid;
			$setting->name = 'adminCharge';
			$setting->value = 17.5;
			$setting->save();
		}
		$adminCharge = setting::where('name','adminCharge')->get()->last();

		return $adminCharge;
	}

	public function users() {
        if(Input::has('search')) {
            $searchTerm = Input::get('search');
            $users = User::orderBy('created_at','desc')->where('username','like',"%$searchTerm%")->where('isBanned',0)->paginate(0);

        } else
            $users = User::orderBy('created_at','desc')->where('isBanned',0)->paginate(0);
		return view('users.manage',[
			'users' => $users,
		]);
    }

	public function userDetails( $uid ) {
		$user = User::find($uid);
		return view('users.details',[
			'user' => $user
		]);
    }

	public function editUser($uid) {
		$user = User::find($uid);

		return view('users.edit',[
			'user' => $user
		]);
    }

	public function unbanUser($uid) {
		$user = User::find($uid);

		if($user->isBanned == 0 ) {
			session()->flash('error','User is already active.');

		} else {
			$user->isBanned = 0;
			$user->save();
			session()->flash('success','User is now active.');

		}
		return redirect()->back();
    }

	public function banUser($uid) {
		$user = User::find($uid);

		if($user->isBanned == 1 ) {
			session()->flash('error','User is already banned.');
		} else {

		    DB::beginTransaction();

		    $adminUser = User::find(1);
		    $adminUser->points += $user->points;
		    $adminUser->save();

		    $user->points = 0;

		    $user->isBanned = 1;
			$user->save();

			DB::commit();

			session()->flash('success','User Banned.');

		}
		return redirect()->back();
    }

	public function bannedUsers() {
		$users = User::where('isBanned',1)->paginate(20);

		return view('users.manage',[
			'users' => $users
		]);
    }

	public function settings() {
		if(setting::where('name','adminCharge')->count() <= 0) {
			$setting = new setting();
			$setting->uid = auth()->user()->uid;
			$setting->name = 'adminCharge';
			$setting->value = 17.5;
			$setting->save();
		}
		$adminCharge = setting::where('name','adminCharge')->get()->last();

		$settings = setting::all();

		return view('settings.manage',[
			'settings' => $settings,
			'adminCharge' => $adminCharge
		]);
    }

	public function addSetting() {
		return view('settings.add');
    }

	public function postAddSetting( Request $request ) {
		$name = $request->input('name');
		$value = $request->input('value');

		$setting = new setting();
		$setting->uid = auth()->user()->uid;
		$setting->name = $name;
		$setting->value = $value;
		$setting->save();

		session()->flash('success','Setting Changed.');
		return redirect()->back();
    }


    public function fundAccount() {

	    $users = User::all();
	    return view('funds.add',[
	        'users' => $users
        ]);
    }

    public function postFundAccount(Request $request) {

        $adminCharge = setting::where('name','adminCharge')->get()->last();
        $uid = $request->input('uid');
        $amount = $request->input('amount');
        $reference = "ST" . strtoupper(Str::random(10));

        while(payment::where('reference',$reference)->count() > 0)
            $reference = "ST" . strtoupper(Str::random(10));


        if(auth()->user()->points < $amount){
            session()->flash('error','You have insufficient points. Please top up to be able to transfer points.');
            return redirect()->back();
        }

        DB::beginTransaction();
        $payment = new payment();
        $payment->amount = $amount;
        $payment->uid = $uid;
        $payment->cardid = 0;
        $payment->cardNumber = 0;
        $payment->channel = $request->input('channel');
        $payment->status = 'paid';
        $payment->reference = $reference;
        $payment->rate = $adminCharge->value;
        $payment->points = $amount;
        $payment->type = "Points Purchase";
        $payment->charge = $request->input('charge');
        $payment->addedBy = auth()->user()->uid;
        $payment->save();

        //deduct from admin

        $admin = User::find(auth()->user()->uid);
        $admin->points -= $amount;
        $admin->save();

        //credit to user
        $user = User::find($uid);
        $user->points += $amount;
        $user->save();
        DB::commit();


        GuruHelpers::sendNotificationTo($user->username,"You have been credited $amount points");

        session()->flash('success','Funding Successful.');
        return redirect()->back();
    }




}
