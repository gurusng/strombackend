<?php

namespace App\Http\Controllers;

use App\task;
use App\taskMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class UserController extends Controller
{

	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		if(auth()->user()->role == 'admin') return redirect('admin/dashboard');
		else {
		    session()->flash('error','You are not allowed to login here.');
		    auth()->logout();
		    return redirect('login');
        }

		$tasks = task::where('uid',auth()->user()->uid)->get()->sortByDesc('created_at');
		return view('dashboard.users.dashboard',[
			'tasks'  => $tasks
		]);


	}

	public function addTask() {
		return view('dashboard.users.tasks.add');
	}

	public function tasks() {
		if(Input::has('status')){
			$status = Input::get('status');
			$tasks = task::where('status', $status)->get()->sortByDesc('created_at');
		} else
		$tasks = task::all();
		return view('dashboard.users.tasks.manage',[
			'tasks' => $tasks
		]);
	}

	public function postAddTask( Request $request ) {
		$expiry = $request->input('expiry');
		$expiry = Carbon::createFromFormat("m/d/Y H:i",$expiry);


		$task = new task();
		$task->uid = auth()->user()->uid;
		$task->name = $request->input('name');
		$task->shortDescription = $request->input('shortDescription');
		$task->longDescription = $request->input('longDescription');
		$task->expiry = $expiry;
		$task->points = $request->input('points');
		$task->maxUsers = $request->input('maxUsers');
		$task->verificationDuration = $request->input('verificationDuration');
		$task->type = $request->input('type');
		$task->status = "In Progress";
		$task->isSuspended = 0;
		$task->save();

//		if($request->hasFile('images')){
//			$images = $request->file('images');
//
//			foreach($images as $image){
//				$taskMedia = new taskMedia();
//				$taskMedia->type = "image";
////				$taskMedia->url = $url;
//				$taskMedia->save();
//			}
//		}


		// collect pictures for the request
		$images = $request->file('images');

		foreach($images as $image){

			$rand           = auth()->user()->uid.  Str::random(5) . \Carbon\Carbon::now()->timestamp;
			$inputFileName = $image->getClientOriginalName();
			$image->move( "uploads/tasks/community", $rand . $inputFileName );
			$url = url('uploads/tasks/community/' . $rand . $inputFileName);

			$taskMedia = new taskMedia();
			$taskMedia->tid = $task->tid;
			$taskMedia->type = "image";
			$taskMedia->url = $url;
			$taskMedia->save();
		}

		session()->flash('success','Task Created');
		return redirect()->back();

	}

	public function suspendTask( $tid ) {
		$task = task::find($tid);
		$task->isSuspended = 1;
		$task->save();
		session()->flash('success','Task Suspended.');
		return redirect()->back();
	}

	public function resumeTask( $tid ) {
		$task = task::find($tid);
		$task->isSuspended = 0;
		$task->save();
		session()->flash('success','Task Resumed.');
		return redirect()->back();
	}

}
