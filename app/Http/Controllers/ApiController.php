<?php

namespace App\Http\Controllers;

use App\card;
use App\cashout;
use App\confirmation;
use App\Mail\confirmationMail;
use App\Mail\resetPasswordMail;
use App\payment;
use App\rating;
use App\setting;
use App\task;
use App\taskMedia;
use App\taskUser;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ApiController extends Controller
{

    public $pointsPerReferral = 20;

	public function signIn(Request $request ) {


		try{

			$email = $request->input('email');
			$password = $request->input('password');

			if(empty($email)) return response(array("message" => "Email is required"),200);
			if(empty($password)) return response(array("message" => "Password is required"),200);

			//check for existing email
			$users =  User::where('email',$email)->count();
			if($users <= 0 ) return response(array("message" => "Email is not registered with us"),200);

			$user =  User::where('email',$email)->first();

			if($user->isBanned == 1) return response(array("message" => "You have been banned from using this application."),200);


			$userCount = user::where('email', $email)->count();

			if ( $userCount > 0 ){
                $user = user::where('email', $email)->first();

                if (password_verify($password,$user->password)){

                    try{$user->Ads;}catch (\Exception $exception){}
                    try{$user->Tasks;}catch (\Exception $exception){}
                    try{$user->Tasks;}catch (\Exception $exception){}
                    try{$user->Cards;}catch (\Exception $exception){}
                    try{$user->Payments;}catch (\Exception $exception){}
                    try{$user->Cashouts;}catch (\Exception $exception){}

                    $referred = User::where('referredBy',$user->refCode)->get();
                    $user['referred'] = $referred;
                    $user['pointsPerReferral'] = $this->pointsPerReferral;


                    return response(array('message'=>'Successful','data'=> $user),200);
				}else{
					return response(array("message"=>'Wrong credentials.'),200);
				}
			}

		}catch (\Exception $exception){

		    Bugsnag::notifyException($exception);
			return response(array('message'=>'Something went wrong. Please try again.'),500);

		}


	}


	public function signUp( Request $request ) {
		$name = $request->input('name');
		$username = $request->input('username');
		$phone = $request->input('phone');
		$email = $request->input('email');
		$password = $request->input('password');
		$referredBy = $request->input('referredBy');

		$refCode = Str::random(6);

		while(User::where("refCode",$refCode)->count() > 0){
			$refCode = Str::random(6);
		}

        if ( preg_match('/\s/',$username) || preg_match('/[\W]+/',$username) ){
		    return response( array( "message" => "Special characters and spaces are not allowed for usernames"  ), 200 );
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return response( array( "message" => "Invalid Email. Please cross-check it."  ), 200 );
        }



        if(empty($username)) return response(array("message" => "Username is required"),200);
		if(empty($name)) return response(array("message" => "Name is required"),200);
		if(empty($phone)) return response(array("message" => "Phone is required"),200);
		if(empty($email)) return response(array("message" => "Email is required"),200);
		if(empty($password)) return response(array("message" => "Password is required"),200);
		if(!empty($referredBy)){
		    if(User::where('refCode',$referredBy)->count() <= 0 ) return response( array( "message" => "Invalid referral code"  ), 200 );
        }

		$user = User::where("username",$username)->count();
		if($user > 0)  return response(array("message" => "Username already exists"),200);

		$user = User::where("email",$email)->count();
		if($user > 0)  return response(array("message" => "Email already exists"),200);

		$user = User::where("phone",$phone)->count();
		if($user > 0)  return response(array("message" => "Phone number already exists"),200);


		$emailCode = Str::random(18);
		$phoneCode = self::random_number(6);

		try{

        DB::beginTransaction();
		$user = new User();
		$user->username = strtolower($username);
		$user->name = $request->input('name');
		$user->phone = $request->input('phone');
		$user->email = $request->input('email');
		$user->password = bcrypt($password);
		$user->points = 0;
		$user->refCode = $refCode;
		$user->referredBy = $request->input('referredBy');
		$user->save();


		$emailConfirmation = new confirmation();
		$emailConfirmation->uid = $user->uid;
		$emailConfirmation->type = 'email';
		$emailConfirmation->code = $emailCode ;
		$emailConfirmation->save();

		$confirmation = new confirmation();
		$confirmation->uid = $user->uid;
		$confirmation->type = 'phone';
		$confirmation->code = $phoneCode;
		$confirmation->save();

		DB::commit();
//		try{
//			$this->sendSms($phone,"Your STROM verification code is " . $phoneCode);
//		}catch (\Exception $exception){
//		    Bugsnag::notifyException($exception);
//        }

        try{
		    Mail::to($user->email)->send(new confirmationMail($emailConfirmation));
        }catch (\Exception $exception){
		    Bugsnag::notifyException($exception);
        }



		return response( array( "message" => "Successful", "data" => $user  ), 200 );

        }catch (\Exception $exception){
            Bugsnag::notifyException($exception);
		    return response( array( "message" => "Something went wrong. Please try again.",   ), 500 );
        }
	}

	public function verifyCode(Request $request) {

		$code = $request->input('code');
		$uid = $request->input('uid');

		$confirmation = confirmation::where('type','phone')->where('code',$code)->where('uid',$uid)->count();

		if($confirmation <= 0 ) {
			return response( array( "message" => "Invalid Code."  ), 200 );
		}

		$confirmation = confirmation::where('type','phone')->where('code',$code)->where('uid',$uid)->get()->last();

		if($confirmation->code == $code){
			$user = User::find($confirmation->uid);
			$user->isPhoneVerified = 1;
			$user->save();
			return response( array( "message" => "Successful"  ), 200 );
		} else 	return response( array( "message" => "Invalid Code."  ), 200 );


	}

	public function verifyEmailCode(Request $request) {

		$code = $request->input('code');
		$uid = $request->input('uid');

		$confirmation = confirmation::where('type','email')->where('code',$code)->where('uid',$uid)->count();

		if($confirmation <= 0 ) {
			return response( array( "message" => "Invalid Code."  ), 200 );
		}

		$confirmation = confirmation::where('type','email')->where('code',$code)->where('uid',$uid)->get()->last();

		if($confirmation->code == $code){
			$user = User::find($confirmation->uid);
			$user->isEmailVerified = 1;
			$user->email_verified_at = Carbon::now();
			$user->save();
			return response( array( "message" => "Successful"  ), 200 );
		} else 	return response( array( "message" => "Invalid Code."  ), 200 );


	}

	public function resendCode( Request $request ) {
		$uid = $request->input('uid');
		$user = User::find($uid);
		$phone = $user->phone;

        //resend email

        $confirmation = confirmation::where('type','email')->where('uid',$uid)->count();

        if($confirmation <= 0 ) {
            return response( array( "message" => "No verification Email."  ), 200 );
        }

        $confirmation = confirmation::where('type','email')->where('uid',$uid)->get()->last();

        try{
            Mail::to($user->email)->send(new confirmationMail($confirmation));
        }catch (\Exception $exception){
            Bugsnag::notifyException($exception);
        }


        //resend sms
//		$confirmation = confirmation::where('type','phone')->where('uid',$uid)->count();
//
//		if($confirmation <= 0 ) {
//			return response( array( "message" => "No verification SMS."  ), 200 );
//		}
//
//		$confirmation = confirmation::where('type','phone')->where('uid',$uid)->get()->last();
//
//		$this->sendSms($phone,"Your STROM verification code is " . $confirmation->code);
//


        return response( array( "message" => "Successful"  ), 200 );

	}


    public function resetPassword(Request $request) {
        $email = $request->input('email');

        if(empty($email)) return response( array( "message" => "Email is required" ), 200 );

        if(User::where('email',$email)->count() <= 0 ) return response( array( "message" => "User account does not exist"  ), 200 );

        $user = User::where('email',$email)->first();

        $code = self::random_number(6);

        $confirmation = new confirmation();
        $confirmation->uid = $user->uid;
        $confirmation->type = 'email';
        $confirmation->code = $code;
        $confirmation->save();


        Mail::to($user->email)->send(new resetPasswordMail($code));


        return response( array( "message" => "Successful"  ), 200 );


    }


    public function resetPasswordCode(Request $request) {
        $code = $request->input('code');
        $password = $request->input('password');
        $confirmPassword = $request->input('confirmPassword');

        if(empty($code)) return response( array( "message" => "Code is required" ), 200 );
        if(empty($password)) return response( array( "message" => "Password is required" ), 200 );
        if(empty($confirmPassword)) return response( array( "message" => "Password confirmation is required" ), 200 );

        if($password != $confirmPassword) return response( array( "message" => "Password's dont match"  ), 200 );

        if(confirmation::where('code',$code)->count() <= 0 ) return response( array( "message" => "Invalid Code"  ), 200 );

        $confirmation = confirmation::where('code',$code)->first();
        $customer = User::find($confirmation->cid);

        $customer->password = bcrypt($password);
        $customer->save();

        $confirmation->delete();

        return response( array( "message" => "Successful"  ), 200 );

    }



    public function editProfile( Request $request ) {


		$uid = $request->input('uid');
		$name = $request->input('name');
		$username = $request->input('username');
		$phone = $request->input('phone');
		$email = $request->input('email');


		if(empty($uid)) return response(array("message" => "UID is required"),200);

		if(empty($username)) return response(array("message" => "Username is required"),200);
		if(empty($name)) return response(array("message" => "Name is required"),200);
		if(empty($phone)) return response(array("message" => "Phone is required"),200);
		if(empty($email)) return response(array("message" => "Email is required"),200);


		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		if(User::find($uid)->username != $username){
            $user = User::where("username",$username)->count();
            if($user > 0)  return response(array("message" => "Username already exists"),200);
        }

        if(User::find($uid)->email != $email) {
            $user = User::where("email", $email)->count();
            if ($user > 0) return response(array("message" => "Email already exists"), 200);
        }

        if(User::find($uid)->phone != $phone) {
            $user = User::where("phone", $phone)->count();
            if ($user > 0) return response(array("message" => "Phone number already exists"), 200);
        }
		$user = User::find($uid);
		$user->name = $name;
		$user->username = $username;
		$user->email = $email;
		$user->phone = $phone;
		$user->save();

		return response( array( "message" => "Successful", "data" => $user  ), 200 );

	}

	public function changePassword( Request $request ) {

		$uid = $request->input('uid');
		$currentPassword = $request->input('currentPassword');
		$newPassword = $request->input('newPassword');

		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		$user = User::find($uid);

		if(password_verify($currentPassword,$user->password)){
			$user->password = bcrypt($newPassword);
			$user->save();
		} else {
			return response( array( "message" => "Current password is wrong."  ), 200 );
		}


		return response( array( "message" => "Successful", "data" => $user  ), 200 );
	}

	public function tasks( Request $request ) {
		$type = $request->input('type');
		$sort = $request->input('sort');
        if(empty($sort)) $sort = "created_at-desc"; // ensure we are sorting by something if the value is omitted.

        $sortBy = explode("-",$sort)[0];
        $sortTerm = explode("-",$sort)[1];

		if(empty($type)) return response( array( "message" => "Type is required" ), 200 );

		if($type == 'video'){

			$tasks = task::whereIn('type',['videop2v','videov2e'])->where('expiry','>',Carbon::now())->where('isSuspended',0)->orderBy($sortBy,$sortTerm)->paginate(10);

			foreach($tasks as $task){


                    $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();
                    $task['formattedPoints'] = number_format($task->points);
                    try{$task->TaskUsers;}catch (\Exception $exception){}
                    try{$task->Users;}catch (\Exception $exception){}
                    try{$task->Media;}catch (\Exception $exception){}
                    try{$task->User;}catch (\Exception $exception){}

			}

			return response( array( "message" => "Successful", "data" => $tasks  ), 200 );

		}


        if($sortBy == "location" && $type == "community" && $sortTerm != "nigeria"){
            $tasks = task::where('type',$type)->where($sortBy,$sortTerm)->where('expiry','>',Carbon::now())->orderBy("created_at","desc")->paginate(10);
        } else

		$tasks = task::where('type',$type)->where('expiry','>',Carbon::now())->orderBy($sortBy,$sortTerm)->paginate(10);

		foreach($tasks as $task){

                $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();
                $task['formattedPoints'] = number_format($task->points);
                try {$task->TaskUsers;} catch (\Exception $exception) {}
                try {$task->Users;} catch (\Exception $exception) {}
                try {$task->Media;} catch (\Exception $exception) {}
                try {$task->User;} catch (\Exception $exception) {}
		}

		return response( array( "message" => "Successful", "data" => $tasks  ), 200 );
	}


    public function searchVideos(Request $request) {

	    $term = $request->input('term');
        $sort = $request->input('sort');

        $sortBy = explode("-",$sort)[0];
        $sortTerm = explode("-",$sort)[1];


        $users = User::where('username','like',"%$term%")->get();
        $uids = [];
        foreach ($users as $user) array_push($uids,$user->uid);

        $tasks = task::whereIn('type',['videop2v','videov2e'])->where('expiry','>',Carbon::now())->whereIn('uid',$uids)->orWhere('name','like',"%$term%")->whereIn('type',['videop2v','videov2e'])->where('expiry','>',Carbon::now())->orderBy($sortBy,$sortTerm)->paginate(10);


        foreach($tasks as $task){

            $task['formattedExpiry'] = $task->expiry->toDayDateTimeString();
            $task['formattedPoints'] = number_format($task->points);
            try {$task->TaskUsers;} catch (\Exception $exception) {}
            try {$task->Users;} catch (\Exception $exception) {}
            try {$task->Media;} catch (\Exception $exception) {}
            try {$task->User;} catch (\Exception $exception) {}

        }

        return response( array( "message" => "Successful", "data" => $tasks  ), 200 );

    }

	public function taskDetails(Request $request) {
		$tid = $request->input('tid');
		$uid = $request->input('uid');

		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);


		if(empty($tid)) return response(array("message" => "Task ID is required"),200);

		//check if task exists
		$task =  task::where('tid',$tid)->count();
		if($task <= 0 ) return response(array("message" => "Task does not exist."),200);


		$pending = 0;
		$completed = 0;
		$approved = 0;
		$verified = 0;
		$rejected = 0;

		$task = task::find($tid);
		$task['isStarted'] = "false";

		$taskUser = taskUser::where('tid',$tid)->where('uid',$uid)->count();
		if($taskUser > 0) {
			$taskUser = taskUser::where( 'tid', $tid )->where( 'uid', $uid )->first();

			$task['taskUser'] = $taskUser;

			if ( $taskUser->uid == $uid ) {
				$task['isStarted'] = "true";
			}
			if ( $taskUser->uid == $uid && $taskUser->status == "pending" ) {
				$task['userStatus'] = "pending";
			}
			if ( ($taskUser->uid == $uid && $taskUser->status == "completed")  ) {
				$task['userStatus'] = "completed";
			}
			if ( $taskUser->uid == $uid && $taskUser->status == "rejected" ) {
				$task['userStatus'] = "rejected";
			}
		}


		$taskUsers = taskUser::where('tid',$tid)->get();
		foreach($taskUsers as $taskUser){
            if ( $taskUser->status == "approved") {
                $approved++;
            } else if (  $taskUser->status == "completed" ) {
                $completed++;
            } else if ( $taskUser->status == "verified" ) {
                $verified++;
            } else if ( $taskUser->status == "rejected" ) {
                $rejected++;
            } else if ( $taskUser->status == "pending" ) {
                $pending++;
            }

        }

        $tmids = array();
        foreach($task->Media as $media){
            array_push($tmids,$media->tmid);
        }


        $rating = rating::whereIn('tmid',$tmids)->where('uid',$uid)->get();

        if(count($rating) > 0) {
            $task['rated'] = 1;
            $task['stars'] = $rating[0]->rating; // take the rating for the first media
                                                // videos only allow one
        } else {
            $task['rated'] = 0;
        }

        // add more stats for videos
        if($task->type == 'videop2v' || $task->type == 'videov2e'){

            $task['totalViews'] = $task->Media[0]->views;
            $task['noOfViewers'] = count($task->TaskUsers);
            $task['totalEarned'] = $task->points * count($task->TaskUsers);

        }


        $task['pending'] = $pending;
		$task['completed'] = $completed;
		$task['approved'] = $approved;
		$task['verified'] = $verified;
        $task['rejected'] = $rejected;
        $task['formattedPoints'] = number_format($task->points);
        $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();


        try{$task->Media;}catch (\Exception $exception){}
		try{$task->User;}catch (\Exception $exception){}

		return response( array( "message" => "Successful", "data" =>  $task ), 200 );
	}

	public function userTasks( Request $request) {
		// tasks a user has carried out
		$uid = $request->input('uid');
		$type = $request->input('type');
		$sort = $request->input('sort');

		if(empty($sort)) $sort = "created_at-desc"; // ensure we are sorting by something if the value is omitted.

        $sortBy = explode("-",$sort)[0];
        $sortTerm = explode("-",$sort)[1];

        if(empty($type)) return response( array( "message" => "type is required" ), 400 );
		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);


		$userTasks = taskUser::where('uid',$uid)->get();
		$tidArray = array();

		foreach($userTasks as $userTask){
			array_push($tidArray,$userTask->tid);
		}


        if($type == 'video') {

            $tasks = task::whereIn('tid',$tidArray)->whereIn('type', ['videop2v', 'videov2e'])->where('expiry','>',Carbon::now())->orderBy($sortBy, $sortTerm)->paginate(10);

            foreach ($tasks as $task) {

                $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();
                $task['formattedPoints'] = number_format($task->points);
                try{$task->TaskUsers;}catch (\Exception $exception){}
                try{$task->Users;}catch (\Exception $exception){}
                try{$task->Media;}catch (\Exception $exception){}
                try{$task->User;}catch (\Exception $exception){}
            }

            return response(array("message" => "Successful", "data" => $tasks), 200);
        }


        if($sortBy == "location" && $type == "community" && $sortTerm != "nigeria"){
            $tasks = task::whereIn('tid',$tidArray)->where('type',$type)->where($sortBy,$sortTerm)->where('expiry','>',Carbon::now())->orderBy("created_at","desc")->paginate(10);
        } else

            $tasks = task::whereIn('tid',$tidArray)->where('type',$type)->where('expiry','>',Carbon::now())->orderBy($sortBy,$sortTerm)->paginate(10);

        foreach($tasks as $task){
            $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();
            $task['formattedPoints'] = number_format($task->points);

            $pending = 0;
            $completed = 0;
            $approved = 0;
            $verified = 0;
            $rejected = 0;


            foreach( $task->TaskUsers as $taskUser){
                if($taskUser->uid == $uid){
                    if ( $taskUser->status == "approved") {
                        $approved++;
                    } else if (  $taskUser->status == "completed" ) {
                        $completed++;
                    } else if ( $taskUser->status == "verified" ) {
                        $verified++;
                    } else if ( $taskUser->status == "rejected" ) {
                        $rejected++;
                    } else if ( $taskUser->status == "pending" ) {
                        $pending++;
                    }

                }

            }

            $task['notifications'] = $approved;


            try{$task->Users;}catch (\Exception $exception){}
            try{$task->Media;}catch (\Exception $exception){}
            try{$task->User;}catch (\Exception $exception){}
        }


		return response( array( "message" => "Successful", "data" => $tasks  ), 200 );


	}

	public function userAds( Request $request ) {

		// tasks a user paid for
		$uid = $request->input('uid');
		$type = $request->input('type');
		$sort = $request->input('sort');

		if(empty($sort)) $sort = "created_at-desc"; // ensure we are sorting by something if the value is omitted.

        $sortBy = explode("-",$sort)[0];
        $sortTerm = explode("-",$sort)[1];


		if(empty($type)) return response( array( "message" => "type is required" ), 400 );
		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);


        if($type == 'video') {

            $tasks = task::whereIn('type', ['videop2v', 'videov2e'])->where('uid',$uid)->orderBy($sortBy, $sortTerm)->paginate(10);


            foreach($tasks as $task){

                $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();
                $task['formattedPoints'] = number_format($task->points);
                $task['uniqueViewers'] = count($task->TaskUsers);
                try{$task->TaskUsers;}catch (\Exception $exception){}
                try{$task->Users;}catch (\Exception $exception){}
                try{$task->Media;}catch (\Exception $exception){}
                try{$task->User;}catch (\Exception $exception){}
            }

            return response(array("message" => "Successful", "data" => $tasks), 200);
        }


        if($sortBy == "location" && $type == "community" && $sortTerm != "nigeria"){
            $tasks = task::where('uid',$uid)->where('type',$type)->where($sortBy,$sortTerm)->where('expiry','>',Carbon::now())->orderBy("created_at","desc")->paginate(10);
        } else
            $tasks = task::where('uid',$uid)->where('type',$type)->orderBy($sortBy,$sortTerm)->paginate(10);

        foreach($tasks as $task){

            $task['formattedExpiry'] = $task->expiry->toDayDateTimeString();
            $task['formattedPoints'] = number_format($task->points);

            $pending = 0;
            $completed = 0;
            $approved = 0;
            $verified = 0;
            $rejected = 0;


            foreach( $task->TaskUsers as $taskUser){
                if ( $taskUser->status == "approved") {
                    $approved++;
                } else if (  $taskUser->status == "completed" ) {
                    $completed++;
                } else if ( $taskUser->status == "verified" ) {
                    $verified++;
                } else if ( $taskUser->status == "rejected" ) {
                    $rejected++;
                } else if ( $taskUser->status == "pending" ) {
                    $pending++;
                }

            }

            $task['notifications'] = $pending + $completed;

            try{$task->Users;}catch (\Exception $exception){}
            try{$task->Media;}catch (\Exception $exception){}
            try{$task->User;}catch (\Exception $exception){}
        }

		return response( array( "message" => "Successful", "data" => $tasks  ), 200 );

	}

	public function userAdDetailsApproval( Request $request) {
		$tid = $request->input('tid');
		$sort = $request->input('sort');

		if(empty($sort)) $sort = "created_at"; // ensure we are sorting by something if the value is omitted.
		if(empty($tid)) return response(array("message" => "Task ID is required"),200);

		//check for existing task
		$tasks =  task::where('tid',$tid)->count();
		if($tasks <= 0 ) return response(array("message" => "Task does not exist"),200);

		$task = task::find($tid);
		try{$task->User;}catch (\Exception $exception){}
		$users = taskUser::where('tid',$tid)->where('status',"pending")->orderBy($sort)->paginate(10);

		foreach($users as $user) {
			try{$user->User;}catch (\Exception $exception){}
		}

        $pending = 0;
        $completed = 0;
        $approved = 0;
        $verified = 0;
        $rejected = 0;

        $taskUsers = taskUser::where('tid',$tid)->get();
        foreach($taskUsers as $taskUser){
            if ( $taskUser->status == "approved") {
                $approved++;
            } else if (  $taskUser->status == "completed" ) {
                $completed++;
            } else if ( $taskUser->status == "verified" ) {
                $verified++;
            } else if ( $taskUser->status == "rejected" ) {
                $rejected++;
            } else if ( $taskUser->status == "pending" ) {
                $pending++;
            }

        }

        $task['pending'] = $pending;
        $task['completed'] = $completed;
        $task['approved'] = $approved;
        $task['verified'] = $verified;
        $task['rejected'] = $rejected;
        $task['formattedPoints'] = number_format($task->points);
        $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();


        $task["users"] = $users;

		return response( array( "message" => "Successful", "data" => $task  ), 200 );

	}

	public function userAdDetailsVerification( Request $request ) {
		$tid = $request->input('tid');
		$sort = $request->input('sort');

		if(empty($sort)) $sort = "created_at"; // ensure we are sorting by something if the value is omitted.
		if(empty($tid)) return response(array("message" => "Task ID is required"),200);

		//check for existing task
		$tasks =  task::where('tid',$tid)->count();
		if($tasks <= 0 ) return response(array("message" => "Task does not exist"),200);

		$task = task::find($tid);
		try{$task->User;}catch (\Exception $exception){}
		$users = taskUser::where('tid',$tid)->where('status',"completed")->orderBy($sort)->paginate(10);

		foreach($users as $user) {
			try{$user->User;}catch (\Exception $exception){}
		}

        $pending = 0;
        $completed = 0;
        $approved = 0;
        $verified = 0;
        $rejected = 0;

        $taskUsers = taskUser::where('tid',$tid)->get();
        foreach($taskUsers as $taskUser){
            if ( $taskUser->status == "approved") {
                $approved++;
            } else if (  $taskUser->status == "completed" ) {
                $completed++;
            } else if ( $taskUser->status == "verified" ) {
                $verified++;
            } else if ( $taskUser->status == "rejected" ) {
                $rejected++;
            } else if ( $taskUser->status == "pending" ) {
                $pending++;
            }

        }

        $task['pending'] = $pending;
        $task['completed'] = $completed;
        $task['approved'] = $approved;
        $task['verified'] = $verified;
        $task['rejected'] = $rejected;
        $task['formattedPoints'] = number_format($task->points);
        $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();


        $task["users"] = $users;

		return response( array( "message" => "Successful", "data" => $task  ), 200 );

	}

	public function userAdDetails( Request $request ) {

		$tid = $request->input('tid');
		$sort = $request->input('sort');

		if(empty($sort)) $sort = "created_at"; // ensure we are sorting by something if the value is omitted.
		if(empty($tid)) return response(array("message" => "Task ID is required"),200);

		//check for existing task
		$tasks =  task::where('tid',$tid)->count();
		if($tasks <= 0 ) return response(array("message" => "Task does not exist"),200);

		$task = task::find($tid);
		try{$task->User;}catch (\Exception $exception){}
		$users = taskUser::where('tid',$tid)->orderBy($sort)->paginate(10);

		foreach($users as $user) {
			try{$user->User;}catch (\Exception $exception){}
		}

        $pending = 0;
        $completed = 0;
        $approved = 0;
        $verified = 0;
        $rejected = 0;

        $taskUsers = taskUser::where('tid',$tid)->get();
        foreach($taskUsers as $taskUser){
            if ( $taskUser->status == "approved") {
                $approved++;
            } else if (  $taskUser->status == "completed" ) {
                $completed++;
            } else if ( $taskUser->status == "verified" ) {
                $verified++;
            } else if ( $taskUser->status == "rejected" ) {
                $rejected++;
            } else if ( $taskUser->status == "pending" ) {
                $pending++;
            }

        }


        if($task->type == 'videop2v' || $task->type == 'videov2e'){

            $task['totalViews'] = $task->Media[0]->views;
            $task['noOfViewers'] = count($task->TaskUsers);
            $task['totalEarned'] = $task->points * count($task->TaskUsers);

        }

        $task['pending'] = $pending;
        $task['completed'] = $completed;
        $task['approved'] = $approved;
        $task['verified'] = $verified;
        $task['rejected'] = $rejected;
        $task['formattedPoints'] = number_format($task->points);
        $task['formattedExpiry'] = $task->expiry->ToDayDateTimeString();


        $task["users"] = $users;

		return response( array( "message" => "Successful", "data" => $task  ), 200 );

	}

	public function userUpdatedDetails( Request $request ) {

		$uid = $request->input('uid');

		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		$user = User::find($uid);

		try{$user->Ads;}catch (\Exception $exception){}
		try{$user->Tasks;}catch (\Exception $exception){}
		try{$user->Cards;}catch (\Exception $exception){}
		try{$user->Payments;}catch (\Exception $exception){}
		try{$user->Cashouts;}catch (\Exception $exception){}


		// get notifications


        $pending = 0;
        $completed = 0;
        $approved = 0;
        $verified = 0;
        $rejected = 0;

        $taskUsers = taskUser::where('uid',$uid)->get();
        foreach($taskUsers as $taskUser){
            if($taskUser->uid == $uid){
                if ( $taskUser->status == "approved") {
                    $approved++;
                } else if (  $taskUser->status == "completed" ) {
                    $completed++;
                } else if ( $taskUser->status == "verified" ) {
                    $verified++;
                } else if ( $taskUser->status == "rejected" ) {
                    $rejected++;
                } else if ( $taskUser->status == "pending" ) {
                    $pending++;
                }
            }

        }
        $user['taskNotifications'] = $approved;

        // reset for ad counts
        $pending = 0;
        $completed = 0;
        $approved = 0;
        $verified = 0;
        $rejected = 0;


        $ads = task::where('uid',$uid)->get();
        $tids = array();
        foreach ($ads as $ad) array_push($tids,$ad->tid);

        $adUsers = taskUser::whereIn('tid',$tids)->get();

        foreach($adUsers as $taskUser){
            if ( $taskUser->status == "approved") {
                $approved++;
            } else if (  $taskUser->status == "completed" ) {
                $completed++;
            } else if ( $taskUser->status == "verified" ) {
                $verified++;
            } else if ( $taskUser->status == "rejected" ) {
                $rejected++;
            } else if ( $taskUser->status == "pending" ) {
                $pending++;
            }

        }

        $user['adNotifications'] = $pending + $completed;


        // end notifications count



        $referred = User::where('referredBy',$user->refCode)->get();
		$user['referred'] = $referred; // 100 points per user
        $user['pointsPerReferral'] = $this->pointsPerReferral;

		return response( array( "message" => "Successful", "data" => $user  ), 200 );
	}

	public function changeProfilePhoto( Request $request ) {

		$uid = $request->input('uid');

		if(!$request->hasFile('image')) return response( array( "message" => "No image attached."  ), 200 );

		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		$user = User::find($uid);



		if($request->hasFile('image')){

		    try{
                if(isset($user->image)){ // if there was a photo,delete it
                    $urlArray = explode("/", $user->image);
                    $fileName = $urlArray[3]. "/" . $urlArray[4] . "/" . $urlArray[5];

                    if($fileName != "images/default-image.jpg"){
                        unlink($fileName);	 // delete the last image
                    }

                }

            }catch (\Exception $exception){
		        Bugsnag::notifyException($exception);
            }

			$rand = Str::random(6);
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('uploads/profileImages',$user->uid . Carbon::now()->timestamp . $rand . $filename);
			$url = url('uploads/profileImages/' . $user->uid . Carbon::now()->timestamp . $rand . $filename);
		} else {
			$url = url('images/default-image.jpg');
		}


		$user->image = $url;
		$user->save();
		return response( array( "message" => "Successful", "data" => $user  ), 200 );
	}

	public function resolveAccountName( Request $request ) {
		$accountNumber = $request->input('accountNumber');
		$accountBank = $request->input('accountBank');

		$bankCode = $this->findBankCode($accountBank);
		$accountVerificationUrl = env('ACCOUNT_VERIFCATION_URL');

		$publicKey = env('PUBLIC_KEY');
		$data = [
			"recipientaccount" => $accountNumber,
			"destbankcode" => $bankCode,
			"PBFPubKey" => $publicKey
		];

		$verify = new client();
		$post = $verify->request('POST', $accountVerificationUrl,[
			'form_params' => $data
		]);

		$response = $post->getBody()->getContents();

		return $response;

	}


	public function deleteCard( Request $request ) {
		$cardid = $request->input('cardid');

		if(!isset($cardid)) return response(array("message" => "CARDID is required"),400);
		if(card::where('cardid',$cardid)->count() <= 0)
			return response(array("message" => "Card does not exist"),400);

		$card = card::find($cardid);
		$card->delete();

		$user = User::find($card->uid);

		try{$user->Cards;}catch (\Exception $exception){}

		return array("message" => "Successful", "data" => $user);

	}


	public function addCard( Request $request ) {


		$uid = $request->input('uid');

		$cardNumber = $request->input('cardNumber');
		$cvv = $request->input('cvv');
		$expiryMonth = $request->input('expiryMonth');
		$expiryYear = $request->input('expiryYear');
		$pin = $request->input('pin');


		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		$user = User::find($uid);

		//required variables
		$publicKey = env('PUBLIC_KEY');
		$secretKey = env('SECRET_KEY');
		$rate = setting::where('name','adminCharge')->get()->last()->value;
		$chargeUrl = env('CHARGE_URL');
		$ip = $request->ip();

		$name = $user->name;
		$amount = 20;

		$reference = "ST" . strtoupper(Str::random(10));


		//strip spaces for some input
		$cardNumber = $string = str_replace(' ', '', $cardNumber);


		if(empty($name)) return response( array( "message" => "User has to have a name on their profile"  ), 200 );



		$payment = new payment();
		$payment->uid = $uid;
		$payment->cardNumber = substr($cardNumber,strlen($cardNumber)-6);
		$payment->amount = $amount;
		$payment->channel = 'card';
		$payment->status = "Initiated";
		$payment->reference = $reference;
		$payment->points =  $amount;
		$payment->rate = $rate;
		$payment->type = "Card Addition";
		$payment->save();


//begin sunnys method
		$data = [
			'PBFPubKey'   => $publicKey,
			'cardno'      => $cardNumber,
			'cvv'         => $cvv,
			'amount'      => $amount,
			'expirymonth' => $expiryMonth,
			'expiryyear'  => $expiryYear,
			'email'       => $user->email,
			"phonenumber" => $user->phone,
			"firstname"   => $name,
			'IP'          => $ip,
			'txRef'       => $reference
		];


		$key      = $this->getKey( $secretKey );
		$data_req = json_encode( $data );

		$encrypted_post_data = $this->encrypt3Des( $data_req, $key );

		$postData = [
			'client'    => $encrypted_post_data,
			'PBFPubKey' => $publicKey,
			'alg'       => '3DES-24'
		];



		$client   = new client();
		$request  = $client ->request( 'POST', $chargeUrl, [ 'form_params' => $postData ] );
		$response = $request->getBody()->getContents();



		$json_response = json_decode( $response );
		if ( $json_response->status == 'success' and $json_response->data->suggested_auth == 'PIN' ) {


			$data = [
				'PBFPubKey'      => $publicKey,
				'cardno'         => $cardNumber,
				'cvv'            => $cvv,
				'amount'         => $amount,
				'expirymonth'    => $expiryMonth,
				'expiryyear'     => $expiryYear,
				'suggested_auth' => 'PIN',
				'pin'            => $pin,
				'firstname'      => $name,
				'email'          => $user->email,
				"phonenumber" => $user->phone,
				'IP'             => $ip,
				'txRef'          => $reference
			];

			$second_enc_data = json_encode( $data );

			$second_encrypt   = $this->encrypt3Des( $second_enc_data, $key );
			$second_data_post = [
				'client'    => $second_encrypt,
				'PBFPubKey' => $publicKey,
				'alg'       => '3DES-24'

			];

			$post         = new client();
			$client       = $post->request( 'POST', $chargeUrl, [
				'form_params' => $second_data_post
			] );

			$chargeResponse = json_decode($client->getBody()->getContents());

			$responseForApp = array(
				'reference' => $chargeResponse->data->txRef,
				'amount'    => $chargeResponse->data->amount,
				'statuscode'=> $chargeResponse->data->chargeResponseCode,
				'message'   => $chargeResponse->data->chargeResponseMessage,
				'currency'  => $chargeResponse->data->narration,
				'type'      => $chargeResponse->data->paymentType,
				'status'    => $chargeResponse->data->status,
//				'customer'  => $chargeResponse->data->customer->email
			);

			//update payment transaction with reference from quidpay
			$payment->status = "pending";
			$payment->flwReference = $chargeResponse->data->flwRef;
			$payment->save();

			return response( array( "message" => "Successful", "data" =>  $responseForApp ), 200 );



		//foreign card implemenation
		} elseif ( $json_response[ 'status' == 'success' ] and $json_response['data']['suggested_auth'] == 'NOAUTH_INTERNATIONAL' ) {

			$data = [
				'PBFPubKey'      => $publicKey,
				'cardno'         => $cardNumber,
				'cvv'            => $cvv,
				'amount'         => $amount,
				'expirymonth'    => $expiryMonth,
				'expiryyear'     => $expiryYear,
				'email'          => $user->email,
				"phonenumber"    => $user->phone,
				"firstname"      => $name,
				'IP'             => $ip,
				'txRef'          => $reference,
				'suggested_auth' => 'NOAUTH_INTERNATIONAL',
				'billing_zip'    => 'zip',
				'billingcity'    => 'city',
				'billingstate'   => 'state',
				'billingaddress' => 'address',
				'billingcountry' => "Nigeria"
			];

			$second_enc_foreign_data = json_encode( $data );
			$foreign_enc_foreign_data = $this->encrypt3Des( $second_enc_foreign_data, $key );
			$foreign_data_post       = [
				'client'    => $foreign_enc_foreign_data,
				'PBFPubKey' => $publicKey,
				'alg'       => '3DES-24'
			];
			$post                    = new Client();
			$client                  = $post->request( 'POST', $chargeUrl, [
				'form_params' => $foreign_data_post
			] );
			$chargeResponse        = json_decode($client->getBody()->getContents());

			$responseForApp = array(
				'reference' => $chargeResponse->data->txRef,
				'amount'    => $chargeResponse->data->amount,
				'statuscode'=> $chargeResponse->data->chargeResponseCode,
				'message'   => $chargeResponse->data->chargeResponseMessage,
				'currency'  => $chargeResponse->data->narration,
				'type'      => $chargeResponse->data->paymentType,
				'status'    => $chargeResponse->data->status,
//				'customer'  => $chargeResponse->data->customer->email
			);



			return response( array( 'message' => 'Successful', 'data' => $responseForApp ), 200 );


		} else {
			$payment->status = "failed";
			$payment->save();
			return response( array( "message" => "Can't add this card." ), 400 );
		}



	}

	public function authorizePayment( Request $request ) {
		$reference = $request->input('reference');
		$otp = $request->input('otp');

		$chargeAuthUrl = env('CHARGE_AUTHORIZATION_URL');
		$chargeVerificationUrl = env('CHARGE_VERIFICATION_URL');
		$publicKey = env('PUBLIC_KEY');
		$secretKey = env('SECRET_KEY');


		$payment = payment::where('reference',$reference)->count();
		if($payment <= 0) return response( array( "message" => "Invalid Reference Number" ), 200 );
		if(empty($reference)) return response(array("message" => "Reference Number not present. Reference is required."),200);
		if(empty($otp)) return response(array("message" => "OTP not present. OTP is required"),200);

		$payment = payment::where('reference',$reference)->get()->last();


		if($payment->status == "initiated" || $payment->status == "failed" ) {
			$payment->status = "failed";
			$payment->save();
			return response(array("message" => "Transaction error. Please try again."),200);
		}

		$data = [
			'PBFPubKey' => $publicKey,
			"transaction_reference" => $payment->flwReference,
			"otp" => $otp

		];

		$post         = new client();
		try {
			$client = $post->request( 'POST', $chargeAuthUrl, [
				'form_params' => $data
			] );
		} catch ( GuzzleException $e ) {
			return response( array( "message" => "Failed.", "data" => $e->getMessage()   ), 200 );
		}

		$authorizationResponse        = json_decode($client->getBody()->getContents());

		$data = [
			'txref' => $reference,
			'SECKEY' => $secretKey
		];
		$post         = new client();
		try {
			$client = $post->request( 'POST', $chargeVerificationUrl, [
				'form_params' => $data
			] );
		} catch ( GuzzleException $e ) {
			return response( array( "message" => "Failed.", "data" => $e->getMessage()  ), 200 );
		}

		$verificationResponse = json_decode($client->getBody()->getContents());
		$authCode = $verificationResponse->data->card->life_time_token;
		$cardNumber = $verificationResponse->data->card->last4digits;
		$cardType = $verificationResponse->data->card->type;


		if($verificationResponse->data->status != "successful" && $verificationResponse->data->chargecode != "00")
			return response( array( "message" => "Transaction verification failed. Please try again." ), 200 );

		DB::beginTransaction();

		$payment->status = "paid";
		$payment->save();


		$card = new card();
		$card->uid = $payment->User->uid;
		$card->authCode = $authCode;
		$card->cardNumber = $cardNumber;
		$card->cardType = $cardType;
		$card->save();

		//credit the user for the card addition payment
		$user = User::find($payment->uid);
//		$user->points += $payment->amount + 80;
//		$user->save();

		DB::commit();

		try{$user->Cards;}catch (\Exception $exception){}
		try{$user->Payments;}catch (\Exception $exception){}

		$responseForApp = array(
			'reference' => $verificationResponse->data->txref,
			'amount'    => $verificationResponse->data->amount,
			'currency'  => $verificationResponse->data->narration,
			'user'      => $user
		);


		return response( array( "message" => "Successful", "data" => $responseForApp  ), 200 );

	}

	public function startTask( Request $request ) {

		$uid = $request->input('uid');
		$tid = $request->input('tid');

		if(empty($uid)) return response(array("message" => "UID is required"),200);
		if(empty($tid)) return response(array("message" => "TID is required"),200);


		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		//check for existing task
		$task =  task::where('tid',$tid)->count();
		if($task <= 0 ) return response(array("message" => "Task does not exist"),200);

		$task = task::find($tid);
		if(Carbon::now()->greaterThan($task->expiry)) return response( array( "message" => "Task has expired."  ), 200 );

        //ensure users can't carry out their own tasks
        if($task->uid == $uid) return response( array( "message" => "You cannot start a task you created."  ), 200);

        //check for existing task
		$task =  taskUser::where('tid',$tid)->where('uid',$uid)->count();
		if($task > 0 ) return response(array("message" => "You already started this task"),200);


		$code = Str::random(8);

		$code = strtoupper($code);

		while(taskUser::where('code',$code)->count() > 0 ){
			$code = Str::random(8);
		}

		$taskUser = new taskUser();
		$taskUser->tid = $tid;
		$taskUser->uid = $uid;
		$taskUser->code = $code;
		$taskUser->status = "pending";
		$taskUser->save();

		$task = task::find($taskUser->tid);
        $this->sendNotificationTo($task->User->username,"You have a user to approve on '$task->name'");

		return response( array( "message" => "Successful", "data" => $taskUser  ), 200 );
	}

	public function approveTask( Request $request ) {

		$tuid = $request->input('tuid');
		if(empty($tuid)) return response(array("message" => "TUID is required"),200);

		//check for existing task user
		$task =  taskUser::where('tuid',$tuid)->count();
		if($task <= 0 ) return response(array("message" => "TUID does not exist"),200);



		$taskUser = taskUser::find($tuid);
		$task = task::find($taskUser->tid);

		if($taskUser->status == "approved") return response( array( "message" => "Task has already been approved"  ), 200 );

		if($taskUser->status == "completed" ) return response( array( "message" => "Does not require approval. Task has already been completed."  ), 200 );

		$user = User::find($taskUser->Task->uid);

		$numberOfApproved = taskUser::where('tid',$taskUser->tid)->where('status', 'approved')->orWhere('status', 'completed')->where('tid',$taskUser->tid)->count();


		$totalValue = $numberOfApproved * $task->points;

		if(($user->points - $totalValue) < $totalValue ) return response( array( "message" => "You don't have enough balance to approve more users "  ), 200 );

		$taskUser->status = "approved";
		$taskUser->save();

		$user = User::find($taskUser->uid);
		$task = task::find($taskUser->tid);
		$response = $this->sendNotificationTo($user->username, "Task '$task->name' has been approved.");


		return response( array( "message" => "Successful", "data" => $taskUser  ), 200 );

	}

	public function approveAllTasks( Request $request ) {

		$tid = $request->input('tid');

		if(empty($tid)) return response(array("message" => "TID is required"),200);
		//check for existing task
		$task =  task::where('tid',$tid)->count();
		if($task <= 0 ) return response(array("message" => "Task does not exist"),200);

		$task = task::find($tid);

		$tuidArray = [];
		$taskUserArray = [];

		foreach($task->TaskUsers as $taskUser){
			array_push($tuidArray, $taskUser->tuid);
		}


		foreach($tuidArray as $tuid) {

			if ( empty( $tuid ) ) {
				return response( array( "message" => "TUID is required" ), 200 );
			}

			//check for existing task user
			$task = taskUser::where( 'tuid', $tuid )->count();
			if ( $task <= 0 ) {
				return response( array( "message" => "TUID does not exist" ), 200 );
			}


			$taskUser = taskUser::find( $tuid );

			if ( $taskUser->status == "pending" ) {

				$taskUser->status = "approved";
				$taskUser->save();

				array_push($taskUserArray,$taskUser);

				$user = User::find($taskUser->uid);
				$task = task::find($taskUser->tid);
				$response = $this->sendNotificationTo($user->username, "Task '$task->name' has been approved.");


			}

		}
		return response( array( "message" => "Successful", "data" => $taskUserArray  ), 200 );

	}


	public function completeTask( Request $request ) {

		$tuid = $request->input('tuid');
		if(empty($tuid)) return response(array("message" => "TUID is required"),200);
		//check for existing task user
		$task =  taskUser::where('tuid',$tuid)->count();
		if($task <= 0 ) return response(array("message" => "TUID does not exist"),200);



		$taskUser = taskUser::find($tuid);

		if($taskUser->status == "pending") return response( array( "message" => "You have not been approved to carry out this task"  ), 200 );

		if($taskUser->status == "completed") return response( array( "message" => "Task has already been completed"  ), 200 );

		$taskUser->status = "completed";
		$taskUser->save();

        $task = task::find($taskUser->tid);
        $this->sendNotificationTo($task->User->username,"You have a user to verify on '$task->name'");

		return response( array( "message" => "Successful", "data" => $taskUser  ), 200 );
	}

	public function verifyTask( Request $request ) {

		$tuid = $request->input('tuid');
		if(empty($tuid)) return response(array("message" => "TUID is required"),200);

		//check for existing task user
		$task =  taskUser::where('tuid',$tuid)->count();
		if($task <= 0 ) return response(array("message" => "TUID does not exist"),200);



		$taskUser = taskUser::find($tuid);

		if($taskUser->status == "verified") return response( array( "message" => "Task has already been verified"  ), 200 );

		if($taskUser->status == "pending" || $taskUser->status == "approved" ) return response( array( "message" => "You can only approve a completed task"  ), 200 );


		DB::beginTransaction();
		$points = $taskUser->Task->points;

		$advertiser = User::find($taskUser->Task->uid);
		$advertiser->points = $advertiser->points - $points;
		$advertiser->save();


		$user = User::find($taskUser->uid);
		$user->points = $user->points + $points;
		$user->save();

		$taskUser->status = "verified";
		$taskUser->save();

		DB::commit();


		$user = User::find($taskUser->uid);
		$task = task::find($taskUser->tid);
		$response = $this->sendNotificationTo($user->username, "Task '$task->name' has been verified. You earned $points points");


		return response( array( "message" => "Successful", "data" => $taskUser  ), 200 );

	}

	public function verifyAllTasks( Request $request ) {

		$tid = $request->input('tid');

		if(empty($tid)) return response(array("message" => "TID is required"),200);
		//check for existing task
		$task =  task::where('tid',$tid)->count();
		if($task <= 0 ) return response(array("message" => "Task does not exist"),200);

		$tuidArray = [];
		$taskUserArray = [];

		$task = task::find($tid);

		foreach($task->TaskUsers as $taskUser){
			array_push($taskUserArray, $taskUser->tuid);
		}

		foreach($tuidArray as $tuid) {
			if ( empty( $tuid ) ) {
				return response( array( "message" => "TUID is required" ), 200 );
			}

			//check for existing task user
			$task = taskUser::where( 'tuid', $tuid )->count();
			if ( $task <= 0 ) {
				return response( array( "message" => "TUID does not exist" ), 200 );
			}


			$taskUser = taskUser::find( $tuid );

			if ( $taskUser->status != "verified" && $taskUser->status == "completed" ) {


				try {
					DB::beginTransaction();
					$points             = $taskUser->Task->points;
					$advertiser         = User::find( $taskUser->Task->uid );
					$advertiser->points -= $points;
					$advertiser->save();

					$user         = User::find( $taskUser->uid );
					$user->points += $points;
					$user->save();

					$taskUser->status = "verified";
					$taskUser->save();

					DB::commit();

					$user = User::find($taskUser->uid);
					$task = task::find($taskUser->tid);
					$response = $this->sendNotificationTo($user->username, "Task '$task->name' has been verified. You earned $points points");


					array_push( $taskUserArray, $taskUser );
				} catch ( \Exception $exception ) {
				    Bugsnag::notifyException($exception);
				}

			}
		}

		return response( array( "message" => "Successful", "data" => $taskUserArray  ), 200 );

	}


	public function rejectTask( Request $request ) {

		$tuid = $request->input('tuid');
		if(empty($tuid)) return response(array("message" => "TUID is required"),200);

		//check for existing task user
		$task =  taskUser::where('tuid',$tuid)->count();
		if($task <= 0 ) return response(array("message" => "TUID does not exist"),200);



		$taskUser = taskUser::find($tuid);

		if($taskUser->status == "rejected") return response( array( "message" => "Task has already been rejected"  ), 200 );

//		if($taskUser->status == "pending" ) return response( array( "message" => "You can only reject a completed task"  ), 200 );

		if($taskUser->status == "verified" ) return response( array( "message" => "You can not reject an approved task"  ), 200 );


		$taskUser->status = "rejected";
		$taskUser->save();

		try{$taskUser->User;}catch (\Exception $exception){}


		return response( array( "message" => "Successful", "data" => $taskUser  ), 200 );

	}

	public function addVideoTask( Request $request ) {

		$uid = $request->input('uid');
		$name = $request->input('name');

		$longDescription = $request->input('longDescription');
		$points = $request->input('points');
		$maxUsers = $request->input('maxUsers');
		$verificationDuration = $request->input('verificationDuration');
		$video = $request->file('video');
		$type = $request->input('type');

		$expiry = $request->input('expiry');

		if(empty($name)) return response(array("message" => "Task name is required"),200);
		if(empty($longDescription)) return response(array("message" => "Long description is required"),200);
		if(empty($points)) return response(array("message" => "Number of points is required"),200);
		if(empty($maxUsers)) return response(array("message" => "Max number of users is required"),200);
		if(empty($verificationDuration)) return response(array("message" => "Duration for system verification is required"),200);
		if(empty($uid)) return response(array("message" => "UID is required"),200);
		if(empty($type)) return response( array( "message" => "Video type is required" ), 400 );
		if(empty($expiry)) return response( array( "message" => "Expiry is required" ), 200 );

		if(User::where('uid',$uid)->count() <= 0 ) return response( array( "message" => "User does not exist"  ), 200 );

		$user = User::find($uid);

        if($user->points < $points) return response( array( "message" => "You don't have sufficient points. Please buy points."  ), 200 );

		if($type == 'videop2v' && ($user->points < 500) ) return response( array( "message" => "You need to have at least 500 points to be able to post 'Pay to View' videos"  ), 200 );

		if($type != "videop2v" && $type !=  "videov2e") return response( array( "message" => "You sent a wrong parameter for video type"  ), 200 );

		if(!$request->has('video')) return response( array( "message" => "You must upload a video"  ), 200 );

		$filename = $uid . Carbon::now()->timestamp . Str::random(6) .  trim($video->getClientOriginalName() ) ;
		$filepath = 'uploads/video/' . $filename;
		$video->move("uploads/video", $filename);
		$videoUrl = url($filepath);


		$picName = str_slug($filename) . ".png";

		exec("ffmpeg -y -i '$filepath' -ss  5 -vframes 1 $picName");

		if(!is_dir("uploads/thumbnails")) mkdir('uploads/thumbnails');

		if(file_exists($picName)){
			rename($picName, "uploads/thumbnails/$picName");

			$imageUrl = url("uploads/thumbnails/$picName");
		} else {
			$imageUrl = url("images/default-image.jpg");

		}

		$expiry = Carbon::createFromFormat("Y-m-d",$expiry);
        if($expiry->lessThan(Carbon::now())) return response( array( "message" => "Expiry date has to be in future"  ), 200 );


		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		DB::beginTransaction();

		$task = new task();
		$task->uid = $uid;
		$task->name = $name;
		$task->shortDescription =  str_limit($longDescription,150);
		$task->longDescription = $longDescription;
		$task->expiry = $expiry;
		$task->points = $points;
		$task->maxUsers = $maxUsers;
		$task->verificationDuration = $verificationDuration;
		$task->type = $type;
		$task->status = "In Progress";
		$task->isSuspended = 1;
		$task->save();

		$taskMedia = new taskMedia();
		$taskMedia->tid = $task->tid;
		$taskMedia->thumbnail = $imageUrl;
		$taskMedia->type = 'video';
		$taskMedia->url = $videoUrl;
		$taskMedia->save();

		DB::commit();

		try{$task->User;}catch (\Exception $exception){}
		try{$task->Media;}catch (\Exception $exception){}

		return response( array( "message" => "Successful", "data" => $task  ), 200 );
	}

	public function addSocialTask( Request $request ) {

		$uid = $request->input('uid');
		$name = $request->input('name');
		$longDescription = $request->input('longDescription');
		$points = $request->input('points');
		$maxUsers = $request->input('maxUsers');
		$verificationDuration = $request->input('verificationDuration');

		$expiry = $request->input('expiry');

		if(empty($name)) return response(array("message" => "Task name is required"),200);
		if(empty($longDescription)) return response(array("message" => "Long description is required"),200);
		if(empty($points)) return response(array("message" => "Number of points is required"),200);
		if(empty($maxUsers)) return response(array("message" => "Max number of users is required"),200);
		if(empty($verificationDuration)) return response(array("message" => "Duration for system verification is required"),200);
		if(empty($uid)) return response(array("message" => "UID is required"),200);
		if(empty($expiry)) return response( array( "message" => "Expiry is required" ), 200 );

        if($points < 10) return response( array( "message" => "The minimum number of points to offer for social tasks is 10" ), 200 );

        //check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		$user = User::find($uid);

		if($user->points < $points) return response( array( "message" => "You don't have sufficient points. Please fund your account."  ), 200 );
        if($user->points < ( 5 * $points) ) return response( array( "message" => "You must have enough points to pay the first 5 people before your task can be published."  ), 200 );


		$expiry = Carbon::createFromFormat("Y-m-d",$expiry);

		if($expiry->lessThan(Carbon::now())) return response( array( "message" => "Expiry date has to be in future"  ), 200 );

		$task = new task();
		$task->uid = $uid;
		$task->name = $name;
		$task->shortDescription =  str_limit($longDescription,150);
		$task->longDescription = $longDescription;
		$task->expiry = $expiry;
		$task->points = $points;
		$task->maxUsers = $maxUsers;
		$task->verificationDuration = $verificationDuration;
		$task->type = "social";
		$task->status = "In Progress";
		$task->isSuspended = 0;
		$task->save();

		try{$task->User;}catch (\Exception $exception){}

		return response( array( "message" => "Successful", "data" => $task  ), 200 );
	}

	public function addCommunityTask( Request $request ) {

		$uid = $request->input('uid');
		$name = $request->input('name');
		$longDescription = $request->input('longDescription');
		$points = $request->input('points');
		$maxUsers = $request->input('maxUsers');
		$verificationDuration = $request->input('verificationDuration');
		$location = $request->input('location');

		$expiry = $request->input('expiry');
//		$expiry = Carbon::createFromFormat("m/d/Y H:i",$expiry);

		// collect pictures for the request
		$images = $request->file('images');

		if(empty($name)) return response(array("message" => "Task name is required"),200);
		if(empty($longDescription)) return response(array("message" => "Long description is required"),200);
		if(empty($points)) return response(array("message" => "Number of points is required"),200);
		if(empty($maxUsers)) return response(array("message" => "Max number of users is required"),200);
		if(empty($verificationDuration)) return response(array("message" => "Duration for system verification is required"),200);
		if(empty($uid)) return response(array("message" => "UID is required"),200);
		if(empty($expiry)) return response( array( "message" => "Expiry is required" ), 200 );
		if(empty($location)) return response( array( "message" => "location is required" ), 200 );
		if($points < 500) return response( array( "message" => "The minimum number of points to offer for this section is 500" ), 200 );


		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		$expiry = Carbon::createFromFormat("Y-m-d",$expiry);
        if($expiry->lessThan(Carbon::now())) return response( array( "message" => "Expiry date has to be in future"  ), 200 );

        $user = User::find($uid);


        if($user->points <  $points ) return response( array( "message" => "You must have enough points to pay the first person before your task can be published."  ), 200 );


        $task = new task();
		$task->uid = $uid;
		$task->name = $name;
		$task->shortDescription =  str_limit($longDescription,150);
		$task->longDescription = $longDescription;
		$task->expiry = $expiry;
		$task->points = $points;
		$task->maxUsers = $maxUsers;
		$task->verificationDuration = $verificationDuration;
		$task->type = "community";
		$task->status = "In Progress";
		$task->isSuspended = 0;
		$task->location = $location;
		$task->save();


		if($request->has('images')) {
            foreach ($images as $image) {

                $rand = $uid . Str::random(5) . \Carbon\Carbon::now()->timestamp;
                $inputFileName = $image->getClientOriginalName();
                $image->move("uploads/tasks/community", $rand . $inputFileName);
                $url = url('uploads/tasks/community/' . $rand . $inputFileName);

                $taskMedia = new taskMedia();
                $taskMedia->tid = $task->tid;
                $taskMedia->type = "image";
                $taskMedia->url = $url;
                $taskMedia->save();
            }

        }


		try{$task->User;}catch (\Exception $exception){}

		return response( array( "message" => "Successful", "data" => $task  ), 200 );

	}

    public function addCommunityTaskImage(Request $request) {
	    $image = $request->file('image');
	    $uid = $request->input('uid');
	    $tid = $request->input('tid');

        if($request->has('image')) {
            $rand = $uid . Str::random(5) . \Carbon\Carbon::now()->timestamp;
            $inputFileName = $image->getClientOriginalName();
            $image->move("uploads/tasks/community", $rand . $inputFileName);
            $url = url('uploads/tasks/community/' . $rand . $inputFileName);

            $taskMedia = new taskMedia();
            $taskMedia->tid = $tid;
            $taskMedia->type = "image";
            $taskMedia->url = $url;
            $taskMedia->save();
        }

        $task = task::find($tid);
        try{$task->TaskMedia;}catch (\Exception $exception){}

        return response( array( "message" => "Successful", "data" => $task  ), 200 );

    }


	public function pendingTasks( Request $request ) {

		//tasks a user has started carrying out

		$uid = $request->input('uid');
		$sort = $request->input('sort');

		if(empty($sort)) $sort = "created_at"; // ensure we are sorting by something if the value is omitted.


		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);


		$taskUsers = taskUser::where('uid',$uid)->get();

		$tidArray = array();
		foreach($taskUsers as $taskUser){
			array_push($tidArray,$taskUser->tid);
		}

		$tasks = task::whereIn('tid',$tidArray)->orderBy($sort,'desc')->paginate(10);

		return response( array( "message" => "Successful", "data" =>  $tasks ), 200 );


	}

	public function playVideo( Request $request ) {
		$tid = $request->input('tid');
		$uid = $request->input('uid');

		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);


		if(empty($tid)) return response(array("message" => "Task ID is required"),200);

		//check if task exists
		$task =  task::where('tid',$tid)->count();
		if($task <= 0 ) return response(array("message" => "Task does not exist."),200);

		$task = task::find($tid);
		$user = User::find($uid);

		$adUser = User::find($task->uid);


		//check for existing task
		$task =  taskUser::where('tid',$tid)->where('uid',$uid)->count();
		if($task > 0 ) {
			$task = task::find($tid);
			//if user has paid before, let him watch.
			return response( array( "message" => "Successful", "data" => $task  ), 200 );
		}



		if( $adUser->uid == $user->uid ) {
			$task = task::find($tid);
			// don't credit a user for his video
			return response( array( "message" => "Successful", "data" => $task  ), 200 );
		}


		$task = task::find($tid);


		if($task->type != "videop2v" && $task->type != "videov2e") return response( array( "message" => "You can only play videos"  ), 200 );

		if($task->type == "videop2v" && $user->points < $task->points) return response( array( "message" => "You don't have enough points to view this video." ), 200 );
		if($task->type == "videov2e" && ($adUser->points < $task->points)) return response( array( "message" => "Advertiser has disabled video." ), 200 );


        // increase views
        foreach($task->Media as $media){
            $taskMedia = taskMedia::find($media->tmid);
            $taskMedia->views = $taskMedia->views + 1;
            $taskMedia->save();
        }


        $code = Str::random(8);

		$code = strtoupper($code);

		while(taskUser::where('code',$code)->count() > 0 ){
			$code = Str::random(8);
		}

		DB::beginTransaction();
		$taskUser = new taskUser();
		$taskUser->tid = $tid;
		$taskUser->uid = $uid;
		$taskUser->code = $code;
		$taskUser->status = "verified";
		$taskUser->save();

		if($task->type == "videop2v"){
			$user->points = $user->points - $task->points; // make the user pay
			$user->save();

			$adUser = User::find($task->uid);
			$adUser->points = $adUser->points + $task->points;
			$adUser->save();


		}

		if($task->type == "videov2e"){

			$adUser = User::find($task->uid);
			$adUser->points = $adUser->points - $task->points;
			$adUser->save();



			$user->points = $user->points + $task->points; // make the user pay
			$user->save();


		}

		DB::commit();

		try{$task->TaskUser;}catch (\Exception $exception){}

		return response( array( "message" => "Successful", "data" => $task  ), 200 );



	}

	public function addRating( Request $request ) {

		$tid = $request->input('tid');
		$uid = $request->input('uid');
		$uRating = $request->input('rating');

		if(empty($uRating)) return response( array( "message" => "Rating is required" ), 200 );
		if(empty($uid)) return response(array("message" => "UID is required"),200);

		if($uRating > 5) return response( array( "message" => "Rating must be between 1 and 5"  ), 200 );
		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);


		if(empty($tid)) return response(array("message" => "Task ID is required"),200);

		//check if task exists
		$task =  task::where('tid',$tid)->count();
		if($task <= 0 ) return response(array("message" => "Task does not exist."),200);

		$task = task::find($tid);

		if($task->type != "videop2v" && $task->type != "videov2e") return response( array( "message" => "You can only rate videos"  ), 200 );



		foreach($task->Media as $media){
			$rating = new rating();
			$rating->tmid = $media->tmid;
			$rating->uid = $uid;
			$rating->rating = $uRating;
			$rating->save();

			$ratings = rating::where('tmid',$media->tmid)->get();
			$totalRatings = 0;
			$totalRatingValue = 0;

			foreach($ratings as $rate){

				$totalRatingValue += $rate->rating;
				$totalRatings++;
			}

			$currentRating = $totalRatingValue / $totalRatings;

			$taskMedia = taskMedia::find($media->tmid);
			$taskMedia->rating = $currentRating;
			$taskMedia->save();
		}

		try{$task->TaskUsers; }catch (\Exception $exception){}
		try{$task->Media;}catch (\Exception $exception){}
		return response( array( "message" => "Successful", "data" => $task  ), 200 );
	}

	public function testNotification() {
		//testing

		$response = $this->sendNotificationTo("toby", "From the api");

		return $response;
		//end testing

	}

	function sendNotificationTo($username, $message){
		$content = array(
			"en" => $message
		);

		$fields = array(
			'app_id' => "f10a8d41-0944-47c1-8974-09644cdfaf55",
			'filters' => array(array("field" => "tag", "key" => "user_name", "relation" => "=", "value" => $username )),
//			'data' => array("foo" => "bar"),
			'contents' => $content
		);

		$fields = json_encode($fields);


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
			'Authorization: Basic ZGE3NTE1MTItODBjZC00ZTI5LWE1OTktZjM1NWVlNjA0N2Q3'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

    public function creditPayments() {
        Artisan::queue("credit:payments");
	}


    public function webPay(Request $request) {
        $amount = $request->input('amount');
        $uid = $request->input('uid');

        $reference = "ST" . strtoupper(Str::random(10));

        while(payment::where('reference',$reference)->count() > 0)
            $reference = "ST" . strtoupper(Str::random(10));

        //required variables
        $rate = setting::where('name','adminCharge')->get()->last()->value;

        $ip = $request->ip();


        if(empty($uid)) return response(array("message" => "UID is required"),200);

        //check for existing user
        $users =  User::where('uid',$uid)->count();
        if($users <= 0 ) return response(array("message" => "User does not exist"),200);


        if(!isset($amount)) return response(array("message" => "Amount is required"),200);



        $payment = new payment();
        $payment->uid = $uid;
        $payment->cardid = 0;
        $payment->cardNumber = 0;
        $payment->amount = $amount;
        $payment->channel = 'web';
        $payment->status = "Initiated";
        $payment->reference = $reference;
        $payment->points =  $amount;
        $payment->rate = $rate;
        $payment->type = "Points Purchase";
        $payment->save();
        $payment['publicKey'] = env('PUBLIC_KEY');
        $payment['data'] = array(
            'publicKey' => env('PUBLIC_KEY'),
            'reference' => $reference
            );
        return response( array( "message" => "Successful", "data" => $payment  ), 200 );

	}

	public function pay( Request $request ) {
		$cardid = $request->input('cardid');
		$amount = $request->input('amount');
		$uid = $request->input('uid');

        $reference = "ST" . strtoupper(Str::random(10));

        while(payment::where('reference',$reference)->count() > 0)
		$reference = "ST" . strtoupper(Str::random(10));

		//required variables
		$rate = setting::where('name','adminCharge')->get()->last()->value;

		$ip = $request->ip();


		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);


		if(!isset($amount)) return response(array("message" => "Amount is required"),200);

		if(!isset($cardid)) return response(array("message" => "CARDID is required"),200);
		if(card::where('cardid',$cardid)->count() <= 0)
			return response(array("message" => "Card does not exist"),200);


		$card = card::find($cardid);


		$payment = new payment();
		$payment->uid = $uid;
		$payment->cardid = $card->cardid;
		$payment->cardNumber = $card->cardNumber;
		$payment->amount = $amount;
		$payment->channel = 'card';
		$payment->status = "Initiated";
		$payment->reference = $reference;
		$payment->points =  $amount;
		$payment->rate = $rate;
		$payment->type = "Points Purchase";
		$payment->save();

		return $this->chargeSavedCard($payment->pid,$cardid,$ip);

	}


	public function chargeSavedCard($pid, $cardid,$ip) {

		if(empty($pid)) return response(array("message" => "PID is required"),200);
		if(payment::where('pid',$pid)->count() <= 0)
			return response(array("message" => "Payment transaction does not exist"),200);

		$payment = payment::find($pid);

		if($payment->status == "paid" ) return response(array("message" => "Payment already credited", "data" => $payment),200);


//		try{
			$card = card::find($cardid);

			$user = $card->User;
			$uid = $user->uid;

			if(!isset($uid)) return response(array("message" => "UID is required"),200);


			$user = User::find($uid); // get the user


			$tokenizeUrl = env('TOKENIZE_URL');
			$token = $card->authCode;
			$secretKey = env('SECRET_KEY');
			$email = $user->email;
			$txRef = $payment->reference;
			$amount = $payment->amount;
			$country = "NG";
			$currency = "NGN";


			$data = [

				'currency' => $currency,
				'country' => $country,
				'ip' => $ip,
				'txRef' => $txRef,
				'email'=> $email,
				'amount'=> $amount,
				'token' => $token,
				'SECKEY'=> $secretKey,

			];



			$tokenize = new client();

			try{

				try {
					$post = $tokenize->request( 'POST', $tokenizeUrl, [
						'form_params' => $data
					] );
				} catch ( GuzzleException $e ) {
					return response( array( "message" => "Failed.", "data" => $e->getMessage()  ), 200 );
				}

				$response = $post->getBody()->getContents();

				$data = json_decode($response);

				if($data->status == "success" ){

					if($data->data->chargeResponseCode == "00"){ // if this is true then we confirm the payment


						$payment = payment::find($pid);

						if($payment->status == "initiated" || $payment->status == "pending") {
							$payment->flwReference  = $data->data->flwRef;
							$payment->cardNumber = $card->cardNumber;
							$payment->charge = $data->data->appfee;
							$payment->status = "paid";
							$payment->save();

//							try{
//								Mail::to($payment->Customer->email)->send(new paymentMail($payment));
//							}catch (\Exception $exception){
//								Bugsnag::notifyException($exception);
//							}


							if ( $payment->type == "Points Purchase" ) {
								$user                     = User::find( $payment->uid );
								$user->points += $payment->amount;
								$user->save();
							}

						}

					}
				} else {
					$payment->status = 'failed';
					$payment->save();
					return array("message" => "Failed","data"=>$payment);
				}


				return array("message" => "Successful","data" => $payment);

			}catch (\Exception $exception){
				$payment->status = 'failed';
				$payment->cardNumber = $card->cardNumber;
				$payment->cardType = $card->cardType;
				$payment->save();


				Bugsnag::notifyException($exception);
				return response(array("message" => "Failed. ","data" => $exception->getMessage()),200);
			}




//		}catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
//			return response(array("message" => "An error occurred"),200);
//		}

	}

	public function verifyTransaction(Request $request) {

		$verificationUrl = env('CHARGE_VERIFICATION_URL');
		$txRef = $request->input('txRef');
		$Secret_key = env('SECRET_KEY');
		$data = [
			'txRef' => $txRef,
			'SECKEY' => $Secret_key,
		];
		$verify = new client();

		try {
			$post = $verify->request( 'POST', $verificationUrl, [
				'form_params' => $data
			] );
		} catch ( GuzzleException $e ) {
			return response( array( "message" => "Failed.", "data" => $e->getMessage()  ), 200 );
		}

		$response = $post->getBody()->getContents();

		return $response;

	}

	public function myReferrals( Request $request ) {
		$uid = $request->input('uid');
		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		$user = User::find($uid);
		$referrals = User::where('referredBy',$user->refCode)->get();

		return response( array( "message" => "Successful", "data" => $referrals  ), 200 );
	}

	public function cashOutTransfer( Request $request ) {

		$uid = $request->input('uid');

		if(empty($uid)) return response(array("message" => "UID is required"),200);

		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);



        $amount = $request->input('amount');
		$accountBank = $request->input('accountBank');
		$accountNumber = $request->input('accountNumber');
		$accountName = $request->input('accountName');
		$bankCode = $this->findBankCode($accountBank);

		if(empty($amount)) return response( array( "message" => "Amount is required" ), 200 );
		if(empty($accountBank)) return response( array( "message" => "Bank name is required" ), 200 );
		if(empty($accountNumber)) return response( array( "message" => "Account number is required" ), 200 );
		if(empty($accountName)) return response( array( "message" => "Account name is required" ), 200 );

        $user = User::find($uid);

        if($user->points < $amount) return response( array( "message" => "You do not have enough balance. Please fund your account or perform tasks."  ), 200 );


		$reference = "SM" . Str::random(8);

		$reference = strtoupper($reference);

		while(cashout::where('reference',$reference)->count() > 0 ){
			$reference =  "SM" . Str::random(8);
		}


		DB::beginTransaction();

		$cashout = new cashout();
		$cashout->uid = $uid;
		$cashout->reference = $reference;
		$cashout->amount = $amount;
		$cashout->type = "transfer";
		$cashout->accountNumber = $accountNumber;
		$cashout->accountName = $accountName;
		$cashout->accountBank = $accountBank;
		$cashout->bankCode = $bankCode;
		$cashout->save();

		DB::commit();

		//crediting would be done on approval
//		$this->makeSingleTransfer($bankCode,$accountNumber,$amount,"Strom Cashout $cashout->cashid", $reference);

		return response( array( "message" => "Successful", "data" => $cashout  ), 200 );


	}


	public function cashOutAirtime( Request $request ) {

		$uid = $request->input('uid');
		$phone = $request->input('phone');
		$amount = $request->input('amount');
		$mobileNetwork = $request->input('mobileNetwork');


		$reference = "SM" . Str::random(8);

		$reference = strtoupper($reference);

		while(cashout::where('reference',$reference)->count() > 0 ){
			$reference =  "SM" . Str::random(8);
		}


		if(empty($uid)) return response(array("message" => "UID is required"),200);
		if(empty($mobileNetwork)) return response( array( "message" => "Network is required" ), 200 );
		if(empty($amount)) return response( array( "message" => "Amount is required" ), 200 );
		if(empty($phone)) return response( array( "message" => "Phone number is required" ), 200 );


		//check for existing user
		$users =  User::where('uid',$uid)->count();
		if($users <= 0 ) return response(array("message" => "User does not exist"),200);

		$adminCharge = setting::where('name','adminCharge')->get()->last()->value;

		$points =  round(((100 - $adminCharge) / 100) * $amount);


        $user = User::find($uid);
        if($user->points < $amount) return response( array( "message" => "You do not have enough balance. Please fund your account or perform tasks."  ), 200 );


        DB::beginTransaction();

        $cashout = new cashout();
        $cashout->uid = $uid;
        $cashout->reference = $reference;
        $cashout->amount = $amount;
        $cashout->type = "airtime";
        $cashout->mobileNetwork = $mobileNetwork;
        $cashout->phoneNumber = $phone;
        $cashout->save();

        DB::commit();


        return response( array( "message" => "Successful", "data" => $cashout  ), 200 );


	}

	public function makeSingleTransfer( $bankCode, $accountNumber, $amount, $narration, $reference ) {

		$url = env('TRANSFER_URL');
		$secretKey = "FLWSECK-76156c412f48a3d1fe4a01cb32abefb5-X";

		$data = array(
			"account_bank" => $bankCode,
			"account_number" => $accountNumber,
			"amount" => $amount,
			"currency" => "NGN",
			"narration" => $narration,
			"reference" => $reference,
			"seckey" => $secretKey
		);


		$bulkPay = new client();


		try {
			$post = $bulkPay->request( 'POST', $url, [
				'form_params' => $data
			] );
		} catch ( GuzzleException $e ) {
			return $e;
		}

		$response = $post->getBody()->getContents();

		$response = json_decode($response);

		$reference       =  $response->data->reference ;

		return $reference;
	}

	public function confirmSingleTransfer( $reference ) {
		$url = env("STAGING_TRANSACTIONS_URL");
		$secretKey = env( 'SECRET_KEY' );


		$data = array(
			"seckey" => $secretKey,
			"transaction_reference" => $reference
		);

		$singlePay = new client();

		$post       = $singlePay->request( 'POST', $url, [
			'form_params' => $data
		] );

		$response = $post->getBody()->getContents();

		return json_encode($response);
	}



	function getKey($seckey){
		$hashedkey = md5($seckey);
		$hashedkeylast12 = substr($hashedkey, -12);

		$seckeyadjusted = str_replace("FLWSECK-", "", $seckey);
		$seckeyadjustedfirst12 = substr($seckeyadjusted, 0, 12);

		$encryptionkey = $seckeyadjustedfirst12.$hashedkeylast12;
		return $encryptionkey;

	}



	function encrypt3Des($data, $key)
	{
		$encData = openssl_encrypt($data, 'DES-EDE3', $key, OPENSSL_RAW_DATA);
		return base64_encode($encData);
	}



	public function findBankCode( $bank ) {

		$data = '{
"status": "success",
"message": "Banks",
"data": [
{
"name": "ACCESS BANK NIGERIA",
"code": "044",
"country": "NG"
},
{
"name": "ACCESS MOBILE",
"code": "323",
"country": "NG"
},
{
"name": "AFRIBANK NIGERIA PLC",
"code": "014",
"country": "NG"
},
{
"name": "Aso Savings and Loans",
"code": "401",
"country": "NG"
},
{
"name": "DIAMOND BANK PLC",
"code": "063",
"country": "NG"
},
{
"name": "Ecobank Mobile",
"code": "307",
"country": "NG"
},
{
"name": "ECOBANK NIGERIA PLC",
"code": "050",
"country": "NG"
},
{
"name": "ENTERPRISE BANK LIMITED",
"code": "084",
"country": "NG"
},
{
"name": "FBN MOBILE",
"code": "309",
"country": "NG"
},
{
"name": "FIDELITY BANK PLC",
"code": "070",
"country": "NG"
},
{
"name": "FIRST BANK PLC",
"code": "011",
"country": "NG"
},
{
"name": "FIRST CITY MONUMENT BANK PLC",
"code": "214",
"country": "NG"
},
{
"name": "GTBank Mobile Money",
"code": "315",
"country": "NG"
},
{
"name": "GTBANK PLC",
"code": "058",
"country": "NG"
},
{
"name": "HERITAGE BANK",
"code": "030",
"country": "NG"
},
{
"name": "KEYSTONE BANK PLC",
"code": "082",
"country": "NG"
},
{
"name": "Parkway",
"code": "311",
"country": "NG"
},
{
"name": "PAYCOM",
"code": "305",
"country": "NG"
},
{
"name": "SKYE BANK PLC",
"code": "076",
"country": "NG"
},
{
"name": "STANBIC IBTC BANK PLC",
"code": "221",
"country": "NG"
},
{
"name": "Stanbic Mobile",
"code": "304",
"country": "NG"
},
{
"name": "STANDARD CHARTERED BANK NIGERIA LIMITED",
"code": "068",
"country": "NG"
},
{
"name": "STERLING BANK PLC",
"code": "232",
"country": "NG"
},
{
"name": "UNION BANK OF NIGERIA PLC",
"code": "032",
"country": "NG"
},
{
"name": "UNITED BANK FOR AFRICA PLC",
"code": "033",
"country": "NG"
},
{
"name": "UNITY BANK PLC",
"code": "215",
"country": "NG"
},
{
"name": "WEMA BANK PLC",
"code": "035",
"country": "NG"
},
{
"name": "ZENITH BANK PLC",
"code": "057",
"country": "NG"
},
{
"name": "ZENITH Mobile",
"code": "322",
"country": "NG"
},
{
"name": "Coronation Merchant Bank",
"code": "559",
"country": "NG"
},
{
"name": "FSDH Merchant Bank Limited",
"code": "601",
"country": "NG"
},
{
"name": "PARRALEX BANK",
"code": "526",
"country": "NG"
},
{
"name": "Providus Bank",
"code": "101",
"country": "NG"
}
]
}';

		$data = json_decode($data);


		foreach($data->data as $item){
			if($item->name == $bank){

				return $item->code;
			}
		}


	}

	static  function random_number($length, $keyspace = '0123456789')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}

	function sendSms($phone,$message){



		$message = urlencode($message);
		file_get_contents("https://www.bulksmsnigeria.com/api/v1/sms/create?dnd=2&api_token=rVY7mjk9AfG2CCx9KdzHkqB1CSVCoyOvNxEvKLdnhEVbtrtcZ7uM8ElPeC7S&from=STROM&to=$phone&body=$message");

//		/* Variables with the values to be sent. */
//		$owneremail="tobennaa@gmail.com";
//		$subacct="dropster";
//		$subacctpwd="dropster";
//		$sendto= $phone; /* destination number */
//		$sender="STROM"; /* sender id */
//
//		$message= $Message;  /* message to be sent */
//
//		/* create the required URL */
//		$url = "http://www.smslive247.com/http/index.aspx?"  . "cmd=sendquickmsg"  . "&owneremail=" . UrlEncode($owneremail)
//		       . "&subacct=" . UrlEncode($subacct)
//		       . "&subacctpwd=" . UrlEncode($subacctpwd)
//		       . "&message=" . UrlEncode($message)
//		       . "&sender=" . UrlEncode($sender)
//		       ."&sendto=" . UrlEncode($sendto)
//		       ."&msgtype=0";
//
//
//		/* call the URL */
//		if ($f = @fopen($url, "r"))  {
//
//			$answer = fgets($f, 255);
//
//			if (substr($answer, 0, 1) == "+") {
//
//				return 1;
//			}
//			else  {
//				return 0;
//			}
//		}
//
//		else  {  return 0;  }
	}




}
