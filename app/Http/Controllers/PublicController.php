<?php

namespace App\Http\Controllers;

use App\confirmation;
use App\helpers\GuruHelpers;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PublicController extends Controller
{
    public $pointsPerReferral = 20;

	public function index() {
		return view('welcome');
    }

	public function confirmEmail( $code, $uid ) {
		$confirmation = confirmation::where('code', $code)->where('type','email')->where('uid',$uid)->count();

		if($confirmation <= 0) {
			session()->flash('error','Invalid code. Please try to confirm your email again from your dashboard.');
			return view('emailConfirmation');
		}

		$confirmation = confirmation::where('code', $code)->where('type','email')->where('uid',$uid)->get()->last();


		if($confirmation->code == $code){
			$user = User::find($uid);
			$user->isEmailVerified = 1;
            $user->email_verified_at = Carbon::now();
			$user->save();

			$confirmations = confirmation::where('type','email')->where('uid',$uid)->get();
			foreach($confirmations as $confirmation) $confirmation->delete(); // remove all email confirmations that are pending.

//            try{
//
//                //credit user for the referral
//                if(!empty($user->referredBy)) {
//
//                    $adminUser = User::find(1);
//                    $adminUser->points -= $this->pointsPerReferral;
//                    $adminUser->save();
//
//                    if($adminUser->points < 5000) GuruHelpers::sendNotificationTo($adminUser->username,"Your points are lower than 5000. Please top up.");
//
//                    $userThatReferred = User::where('refCode', $user->referredBy)->first();
//                    $userThatReferred->points += $this->pointsPerReferral;
//                    $userThatReferred->save();
//                    GuruHelpers::sendNotificationTo($userThatReferred->username,"$user->username just signed up with your referral code. You earned $this->pointsPerReferral points");
//                }
//                //end user crediting
//
//            }catch (\Exception $exception){
//                Bugsnag::notifyException($exception);
//            }

            session()->flash('success','Email Verified.');

		} else {
			session()->flash('error','Invalid code. Please try to confirm your email again from your dashboard.');
		}

		return view('emailConfirmation');

    }

    public function privacyPolicy() {
	    return view('privacy');
    }

    public function terms() {

    }


}
