<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cashout extends Model
{
    protected $primaryKey = 'cashid';
    protected $table = 'cashouts';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
