<?php

namespace App\Console\Commands;

use App\payment;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class cleantransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans out initiated and abandoned transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get payments that's still initated or pending after an hour of creation
        $payments = payment::where('status','initiated')->where('created_at','<',Carbon::now()->subHour())->get();

        $count = 0;
        foreach($payments as $payment){
            $payment->status = 'failed';
            $payment->save();
            $count++;
        }

        $payments = payment::where('status','pending')->where('created_at','<',Carbon::now()->subHour())->get();

        foreach($payments as $payment){
            $payment->status = 'failed';
            $payment->save();
            $count++;
        }

        echo "Cleaned $count transactions";
    }

}
