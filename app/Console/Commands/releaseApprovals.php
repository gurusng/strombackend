<?php

namespace App\Console\Commands;


use App\taskUser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class releaseApprovals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'release:approvals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Releases approvals after 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $numberReleased = 0;
        $taskUsers = taskUser::where('status','approved')->get();

        foreach($taskUsers as $taskUser){
            if($taskUser->created_at < Carbon::now()->subHours(24)){
                $taskUser->delete();
                $numberReleased++;
            }
        }

        echo $numberReleased . " approved users released";

    }
}
