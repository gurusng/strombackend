<?php

namespace App\Console\Commands;

use App\confirmation;
use App\Mail\confirmationMail;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class resendemails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resend:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $confirmations = confirmation::where('type','email')->get();

        foreach($confirmations as $confirmation){

            try{

                $user = User::find($confirmation->uid);

                Mail::to($user->email)->send(new confirmationMail($confirmation));
            }catch (\Exception $exception){
                Bugsnag::notifyException($exception);
            }

        }

    }
}
