<?php

namespace App\Console\Commands;

use App\helpers\GuruHelpers;
use App\task;
use App\taskUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class hideLowBudgetAds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hide:lowbudget';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hides low budget ads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $numberDisabled = 0;
        $tasks = task::where('isSuspended',0)->get();

        foreach($tasks as $task) {

            $adUser = User::find($task->uid);


            if (taskUser::where('tid', $task->tid)->count() > 0) {


                $taskUser = taskUser::where('tid', $task->tid)->get()->last();

                if ($taskUser->created_at < Carbon::now()->subHours(24)) {
                    if ($task->type == "videov2e" && ($adUser->points < $task->points)) {

                        $task->isSuspended = 1;
                        $task->save();
                        $numberDisabled++;
                        GuruHelpers::sendNotificationTo($task->User->username, "Your ad has been hidden till you have enough points.");
                    }
                }


            }
        }

        echo $numberDisabled . " videos were disabled.";

    }
}
