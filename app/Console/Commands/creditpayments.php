<?php

namespace App\Console\Commands;

use App\helpers\GuruHelpers;
use App\payment;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class creditpayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'credit:payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Credits all web payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now();
        $from = $date->year . "-" . $date->month . "-" . $date->subDay()->day;
        $date = Carbon::now();

        $to = $date->year . "-" . $date->month . "-" . $date->day;

        echo $from . "   |    " . $to;


            $secretKey = env('SECRET_KEY');

            $url = env("TRANSACTIONS_URL");

            $postData = [
                "seckey" => $secretKey,
                "from" => $from,
                "to" => $to,
                "currency" => "NGN",
                "status" => "successful"
            ];


            $client   = new client();


            try {
                $request = $client ->request( 'POST', $url, [ 'form_params' => $postData ] );

            } catch ( GuzzleException $exception ) {
                Bugsnag::notifyException($exception);
            }


            $response = $request->getBody()->getContents();


            $response = json_decode( $response );

            if(count($response->data->transactions) > 0) {



                DB::beginTransaction();

                foreach ( $response->data->transactions as $transaction ) {


                    if (payment::where( 'reference', $transaction->transaction_reference )->count() > 0 ) {

                        $payment = payment::where( 'reference', $transaction->transaction_reference )->first();

                        if($payment->status != 'paid') {

                            $payment->flwReference = $transaction->processor_reference;
                            $payment->cardNumber = $transaction->card->bin;
                            $payment->charge = $transaction->rave_fee;
                            $payment->status = "paid";
                            $payment->save();

                            GuruHelpers::sendNotificationTo($payment->User->username, "You just got credited with $payment->amount points");

//							try{
//								Mail::to($payment->Customer->email)->send(new paymentMail($payment));
//							}catch (\Exception $exception){
//								Bugsnag::notifyException($exception);
//							}


                            if ($payment->type == "Points Purchase") {
                                $user = User::where('email', $transaction->customer->customer_email)->first();
                                $user->points += $payment->amount;
                                $user->save();
                            }
                        }

                    }

                }

                DB::commit();

            } else {
                echo 'No transactions. Please verify API key or wait for at least one successful transaction.';
            }


    }
}
