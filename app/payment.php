<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
    protected $primaryKey = 'pid';
    protected $table = 'payments';
    protected $hidden = ['flwReference'];

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }

    public function Staff() {
        return $this->belongsTo(User::class,'addedBy','uid');
    }
}
