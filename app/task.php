<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class task extends Model
{
    protected $primaryKey = 'tid';
    protected $table = 'tasks';

	public function getExpiryAttribute( $value ) {
		return Carbon::createFromFormat("Y-m-d H:i:s",$value);
    }

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }

	public function Media() {
		return $this->hasMany(taskMedia::class,'tid','tid');
    }

	public function TaskUsers() {
		return $this->hasMany(taskUser::class,'tid','tid');
    }

	public function Users() {
		return $this->belongsToMany(User::class,'task_users','tuid','uid','tid');
    }


}
