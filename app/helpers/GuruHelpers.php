<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2/7/19
 * Time: 6:27 AM
 */

namespace App\helpers;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GuruHelpers
{
    public static function makeSingleTransfer( $bankCode, $accountNumber, $amount, $narration, $reference ) {

        $url = env('TRANSFER_URL');
        $secretKey = env('SECRET_KEY');

        $data = array(
            "account_bank" => $bankCode,
            "account_number" => $accountNumber,
            "amount" => $amount,
            "currency" => "NGN",
            "narration" => $narration,
            "reference" => $reference,
            "seckey" => $secretKey
        );


        $bulkPay = new client();


        try {
            $post = $bulkPay->request( 'POST', $url, [
                'form_params' => $data
            ] );
        } catch ( GuzzleException $e ) {
            return $e;
        }

        $response = $post->getBody()->getContents();

        $response = json_decode($response);

        $reference       =  $response->data->reference ;

        return $reference;
    }

    public static function confirmSingleTransfer( $reference ) {
        $url = env("STAGING_TRANSACTIONS_URL");
        $secretKey = env( 'SECRET_KEY' );


        $data = array(
            "seckey" => $secretKey,
            "transaction_reference" => $reference
        );

        $singlePay = new client();

        $post       = $singlePay->request( 'POST', $url, [
            'form_params' => $data
        ] );

        $response = $post->getBody()->getContents();

        return json_encode($response);
    }



    static function getKey($seckey){
        $hashedkey = md5($seckey);
        $hashedkeylast12 = substr($hashedkey, -12);

        $seckeyadjusted = str_replace("FLWSECK-", "", $seckey);
        $seckeyadjustedfirst12 = substr($seckeyadjusted, 0, 12);

        $encryptionkey = $seckeyadjustedfirst12.$hashedkeylast12;
        return $encryptionkey;

    }

    static function checkForValidPayments($ref) {




    }


    static function encrypt3Des($data, $key)
    {
        $encData = openssl_encrypt($data, 'DES-EDE3', $key, OPENSSL_RAW_DATA);
        return base64_encode($encData);
    }



    static public function findBankCode( $bank ) {

        $data = '{
"status": "success",
"message": "Banks",
"data": [
{
"name": "ACCESS BANK NIGERIA",
"code": "044",
"country": "NG"
},
{
"name": "ACCESS MOBILE",
"code": "323",
"country": "NG"
},
{
"name": "AFRIBANK NIGERIA PLC",
"code": "014",
"country": "NG"
},
{
"name": "Aso Savings and Loans",
"code": "401",
"country": "NG"
},
{
"name": "DIAMOND BANK PLC",
"code": "063",
"country": "NG"
},
{
"name": "Ecobank Mobile",
"code": "307",
"country": "NG"
},
{
"name": "ECOBANK NIGERIA PLC",
"code": "050",
"country": "NG"
},
{
"name": "ENTERPRISE BANK LIMITED",
"code": "084",
"country": "NG"
},
{
"name": "FBN MOBILE",
"code": "309",
"country": "NG"
},
{
"name": "FIDELITY BANK PLC",
"code": "070",
"country": "NG"
},
{
"name": "FIRST BANK PLC",
"code": "011",
"country": "NG"
},
{
"name": "FIRST CITY MONUMENT BANK PLC",
"code": "214",
"country": "NG"
},
{
"name": "GTBank Mobile Money",
"code": "315",
"country": "NG"
},
{
"name": "GTBANK PLC",
"code": "058",
"country": "NG"
},
{
"name": "HERITAGE BANK",
"code": "030",
"country": "NG"
},
{
"name": "KEYSTONE BANK PLC",
"code": "082",
"country": "NG"
},
{
"name": "Parkway",
"code": "311",
"country": "NG"
},
{
"name": "PAYCOM",
"code": "305",
"country": "NG"
},
{
"name": "SKYE BANK PLC",
"code": "076",
"country": "NG"
},
{
"name": "STANBIC IBTC BANK PLC",
"code": "221",
"country": "NG"
},
{
"name": "Stanbic Mobile",
"code": "304",
"country": "NG"
},
{
"name": "STANDARD CHARTERED BANK NIGERIA LIMITED",
"code": "068",
"country": "NG"
},
{
"name": "STERLING BANK PLC",
"code": "232",
"country": "NG"
},
{
"name": "UNION BANK OF NIGERIA PLC",
"code": "032",
"country": "NG"
},
{
"name": "UNITED BANK FOR AFRICA PLC",
"code": "033",
"country": "NG"
},
{
"name": "UNITY BANK PLC",
"code": "215",
"country": "NG"
},
{
"name": "WEMA BANK PLC",
"code": "035",
"country": "NG"
},
{
"name": "ZENITH BANK PLC",
"code": "057",
"country": "NG"
},
{
"name": "ZENITH Mobile",
"code": "322",
"country": "NG"
},
{
"name": "Coronation Merchant Bank",
"code": "559",
"country": "NG"
},
{
"name": "FSDH Merchant Bank Limited",
"code": "601",
"country": "NG"
},
{
"name": "PARRALEX BANK",
"code": "526",
"country": "NG"
},
{
"name": "Providus Bank",
"code": "101",
"country": "NG"
}
]
}';

        $data = json_decode($data);


        foreach($data->data as $item){
            if($item->name == $bank){

                return $item->code;
            }
        }


    }

    static  function random_number($length, $keyspace = '0123456789')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    static function sendSms($phone,$message){



        $message = urlencode($message);
        file_get_contents("https://www.bulksmsnigeria.com/api/v1/sms/create?dnd=2&api_token=rVY7mjk9AfG2CCx9KdzHkqB1CSVCoyOvNxEvKLdnhEVbtrtcZ7uM8ElPeC7S&from=STROM&to=$phone&body=$message");

//		/* Variables with the values to be sent. */
//		$owneremail="tobennaa@gmail.com";
//		$subacct="dropster";
//		$subacctpwd="dropster";
//		$sendto= $phone; /* destination number */
//		$sender="STROM"; /* sender id */
//
//		$message= $Message;  /* message to be sent */
//
//		/* create the required URL */
//		$url = "http://www.smslive247.com/http/index.aspx?"  . "cmd=sendquickmsg"  . "&owneremail=" . UrlEncode($owneremail)
//		       . "&subacct=" . UrlEncode($subacct)
//		       . "&subacctpwd=" . UrlEncode($subacctpwd)
//		       . "&message=" . UrlEncode($message)
//		       . "&sender=" . UrlEncode($sender)
//		       ."&sendto=" . UrlEncode($sendto)
//		       ."&msgtype=0";
//
//
//		/* call the URL */
//		if ($f = @fopen($url, "r"))  {
//
//			$answer = fgets($f, 255);
//
//			if (substr($answer, 0, 1) == "+") {
//
//				return 1;
//			}
//			else  {
//				return 0;
//			}
//		}
//
//		else  {  return 0;  }
    }

    static function sendNotificationTo($username, $message){
        $content = array(
            "en" => $message
        );

        $fields = array(
            'app_id' => "f10a8d41-0944-47c1-8974-09644cdfaf55",
            'filters' => array(array("field" => "tag", "key" => "user_name", "relation" => "=", "value" => $username )),
//			'data' => array("foo" => "bar"),
            'contents' => $content
        );

        $fields = json_encode($fields);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ZGE3NTE1MTItODBjZC00ZTI5LWE1OTktZjM1NWVlNjA0N2Q3'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

}
