<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class confirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $confirmation;

    public function __construct($confirmation)
    {
        $this->confirmation = $confirmation;
    }

    public function build()
    {
        return $this->view('emails.confirmationEmail')->subject("Email Confirmation");
    }
}
