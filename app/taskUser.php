<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class taskUser extends Model
{
    protected $primaryKey = 'tuid';
    protected $table = 'task_users';

	public function Task() {
		return $this->belongsTo(task::class,'tid','tid');
    }

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }


}
