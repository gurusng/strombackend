<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class taskMedia extends Model
{
    protected $primaryKey = 'tmid';
    protected $table = 'task_media';


	public function Task() {
		return $this->belongsTo(task::class,'tid','tid');
    }

	public function Rating() {
		return $this->hasMany(rating::class,'tmid','tmid');
    }
}
