<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    	Schema::create('tasks', function(Blueprint $table){
    	    $table->increments('tid');
    	    $table->integer('uid');
    	    $table->string('name');
    	    $table->string('location');
    	    $table->string('shortDescription');
    	    $table->string('longDescription',5000);
    	    $table->string('expiry');
    	    $table->integer('points');
    	    $table->integer('maxUsers');
    	    $table->integer('verificationDuration')->nullable();
    	    $table->string('type'); // social, videop2v, videov2e, community
    	    $table->string('status'); // 'In Progress', 'Completed'
    	    $table->boolean('isSuspended')->default(0);
    	    $table->timestamps();

    	});

    	Schema::create('task_media', function(Blueprint $table){
    	    $table->increments('tmid');
    	    $table->integer('tid');
    	    $table->string('thumbnail')->nullable();
    	    $table->string('type'); // image, video
		    $table->string('url',2000);
		    $table->integer('views')->default(0);
		    $table->decimal('rating',8,2)->default(0);
		    $table->timestamps();
    	});

    	Schema::create('ratings', function(Blueprint $table){
    	    $table->increments('ratid');
    	    $table->integer('tmid');
    	    $table->integer('uid');
    	    $table->timestamps();
    	});

    	Schema::create('task_users', function(Blueprint $table){
    	    $table->increments('tuid');
    	    $table->integer('tid');
    	    $table->integer('uid');
    	    $table->string('code');
    	    $table->string('status'); // pending, approved, completed, verified, rejected
    	    $table->timestamps();
    	});


    	Schema::create('confirmations', function(Blueprint $table){
    		$table->increments('confid');
    		$table->integer('uid');
    		$table->string('type'); // phone,email
    		$table->string('code');
    		$table->timestamps();
    	});

	    Schema::create('users', function (Blueprint $table) {
		    $table->increments('uid');
		    $table->string('username',191)->unique();
		    $table->string('image')->nullable();
		    $table->string('name');
		    $table->string('role')->default('user'); //user, admin
		    $table->string('phone',191)->unique();
		    $table->boolean('isPhoneVerified')->default(0);
		    $table->boolean('isEmailVerified')->default(0);
		    $table->string('email',191)->unique();
		    $table->timestamp('email_verified_at')->nullable();
		    $table->integer('points')->default(0);
		    $table->string('refCode');
		    $table->string('referredBy')->nullable();
		    $table->string('instagram')->nullable();
		    $table->string('facebook')->nullable();
		    $table->string('twitter')->nullable();
		    $table->boolean('isBanned')->default(0);
		    $table->string('password');
		    $table->rememberToken();
		    $table->timestamps();
	    });

	    Schema::create('cards', function(Blueprint $table){
		    $table->increments('cardid');
		    $table->integer('uid');
		    $table->string('authCode');
		    $table->string('cardNumber');
		    $table->string('cardType');
		    $table->timestamps();
	    });

	    Schema::create('referrals', function(Blueprint $table){
	    	$table->increments('refid');
	    	$table->integer('uid');
	    	$table->integer('referred');
	    	$table->integer('points');
	    	$table->timestamps();
	    });

	    Schema::create('cashouts', function(Blueprint $table){
	        $table->increments('cashid');
	        $table->integer('uid');
	        $table->decimal('amount');
	        $table->enum('status',['pending','approved','rejected'])->default('pending');
	        $table->integer('approvedBy')->nullable();
	        $table->enum('type',['airtime','transfer']); // airtime, transfer
		    $table->string('reference');
		    $table->string('accountNumber')->nullable();
		    $table->string('accountName')->nullable();
		    $table->string('accountBank')->nullable();
		    $table->string('bankCode')->nullable();
		    $table->string('mobileNetwork')->nullable();
		    $table->string('phoneNumber')->nullable();
	        $table->timestamps();
	    });

	    Schema::create('settings', function(Blueprint $table){
	    	$table->increments('setid');
	    	$table->integer('uid');
	    	$table->string('name');
	    	$table->string('value');
	    	$table->timestamps();
	    });


	    Schema::create('payments', function(Blueprint $table){
	    	$table->increments('pid');
	    	$table->integer('uid');
	    	$table->integer('addedBy')->nullable();
		    $table->integer('cardid')->nullable();
	    	$table->integer('cardNumber');
		    $table->decimal('amount');
		    $table->string('channel');
		    $table->enum('status',['initiated','pending','failed','paid']);
		    $table->string('reference');
		    $table->string('flwReference')->nullable();
		    $table->decimal('rate');
	    	$table->integer('points');
	    	$table->string('type');
	    	$table->decimal('charge',12,2)->nullable();
	    	$table->timestamps();

	    });


	    Schema::create('password_resets', function (Blueprint $table) {
		    $table->string('email',191)->index();
		    $table->string('token');
		    $table->timestamp('created_at')->nullable();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('users');
	    Schema::dropIfExists('password_resets');

    }
}
