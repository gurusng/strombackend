<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    Schema::create('ratings', function(Blueprint $table){
		    $table->increments('ratid');
		    $table->integer('tmid');
		    $table->integer('uid');
		    $table->integer('rating');
		    $table->timestamps();
	    });

    	//	    Schema::create('payments', function(Blueprint $table){
//		    $table->increments('pid');
//		    $table->integer('uid');
//		    $table->integer('cardid')->nullable();
//		    $table->integer('cardNumber');
//		    $table->decimal('amount');
//		    $table->string('channel');
//		    $table->enum('status',['initiated','pending','failed','paid']);
//		    $table->string('reference');
//		    $table->string('flwReference');
//		    $table->decimal('rate');
//		    $table->integer('points');
//		    $table->string('type');
//		    $table->timestamps();
//
//	    });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
